Distribución de tareas:

Patricio Lopez: 
- Mejora de la edición de terreno en la GUI
- Implementación de los menús nuevos
- Agregar genets sugeridos

Damir Mandakovic: 
- Visualización con imágenes de los Genets (manteniendo la característica de color de piel que traen)
- Modificación que permite recibir Genets más grandes de otros mundos y representarlos.

Miguel Fadic: 
-Test para GenetBuilder.
-Envío y recepción de entidades de mundo.
-Archivo para indicar los nombres de los mundos, su IP y puerto sin tener que recompilar (WorldsConfig.xml).
-Ant para compilar, generar un ejecutable y ejecutar (build.xml).

Xiang Ji: 
- La despliega de texto de información(e.j. Nuevo genet ha nacido con …) dentro de la GUI.
- Nuevo JProfiler

Christian Gaggero: 
- Modificación para la visualización continua de terreno con imágenes (agua, desierto, llanura)
- UML 
- Manual de uso

** Solicitamos que se nos corrija hasta el commit “d5c7008”, subido a la 23:57 PM
** Gracias