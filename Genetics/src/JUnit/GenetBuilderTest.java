package JUnit;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import lifegenetics.core.BiologicalAccuratePhenotype;
import lifegenetics.core.DefaultFemaleRunBehavior;
import lifegenetics.core.DefaultMaleRunBehavior;
import lifegenetics.core.DefaultReproductionBehavior;
import lifegenetics.core.Genet;
import lifegenetics.core.Genet.GenetBuilder;
import lifegenetics.core.Genotype;
import lifegenetics.core.Genotype.GenotypeBuilder;
import lifegenetics.core.DefaultLiveBehavior;
import lifegenetics.core.DefaultMoveBehavior;
import lifegenetics.core.LiveBehavior;
import lifegenetics.core.MoveBehavior;
import lifegenetics.core.Phenotype;
import lifegenetics.core.ReproductionBehavior;
import lifegenetics.core.RunBehavior;
import lifegenetics.core.World;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class GenetBuilderTest {

	GenetBuilder builder;
	
	World world;
	Genotype genotype;
	Phenotype phenotype;
	MoveBehavior moveBehavior;
	LiveBehavior liveBehavior;
	RunBehavior runBehavior;
	ReproductionBehavior reproductionBehavior;
	
	@Before
	public void setUp() throws Exception {
		builder = new GenetBuilder();
		world = new World(10,10);
		genotype = new GenotypeBuilder().build();
		phenotype = new BiologicalAccuratePhenotype();
		moveBehavior = new DefaultMoveBehavior();
		liveBehavior = new DefaultLiveBehavior();
		runBehavior = new DefaultFemaleRunBehavior();
		reproductionBehavior = new DefaultReproductionBehavior();
	}

	@Test
	public void testI() {
		//Default value
		assertTrue(builder.getI()==0);
		
		//Valid value
		builder.i(10);
		assertTrue(builder.getI()==10);
		
		//Invalid value
		builder.i(-1);
		assertTrue(builder.getI()==10);
	}

	@Test
	public void testJ() {
		//Default value
		assertTrue(builder.getJ()==0);
		
		//Valid value
		builder.j(10);
		assertTrue(builder.getJ()==10);
		
		//Invalid value
		builder.j(-1);
		assertTrue(builder.getJ()==10);
	}

	@Test
	public void testHeight() {
		//Default value
		assertTrue(builder.getHeight()==0);
		
		//Valid value
		builder.height(10);
		assertTrue(builder.getHeight()==10);
		
		//Invalid value
		builder.height(-1);
		assertTrue(builder.getHeight()==10);
	}

	@Test
	public void testWidth() {
		//Default value
		assertTrue(builder.getWidth()==0);
		
		//Valid value
		builder.width(10);
		assertTrue(builder.getWidth()==10);
		
		//Invalid value
		builder.width(-1);
		assertTrue(builder.getWidth()==10);
	}

	@Test
	public void testOriginalGroupID() {
		assertTrue(builder.getOriginalGroupID()==0);
		
		builder.originalGroupID(10);
		assertTrue(builder.getOriginalGroupID()==10);
	}

	@Test
	public void testWorld() {
		
		assertNull(builder.getWorld());
		
		builder.world(world);
		assertEquals(world, builder.getWorld());
		
	}

	@Test
	public void testGenotype() {
		assertNull(builder.getGenotype());
		
		builder.genotype(genotype);
		assertEquals(genotype, builder.getGenotype());
	
	}

	@Test
	public void testPhenotype() {
		assertNull(builder.getPhenotype());
		
		builder.phenotype(phenotype);
		assertEquals(phenotype,builder.getPhenotype());
	}

	@Test
	public void testMoveBehavior() {
		assertNull(builder.getMoveBehavior());
		
		builder.moveBehavior(moveBehavior);
		assertEquals(moveBehavior,builder.getMoveBehavior());
	}

	@Test
	public void testLiveBehavior() {
		assertNull(builder.getLiveBehavior());
			
		builder.liveBehavior(liveBehavior);
		assertEquals(liveBehavior,builder.getLiveBehavior());
	}

	@Test
	public void testRunBehavior() {
		assertNull(builder.getRunBehavior());
		
		builder.runBehavior(runBehavior);
		assertEquals(runBehavior,builder.getRunBehavior());
		
		runBehavior = new DefaultMaleRunBehavior();
		builder.runBehavior(runBehavior);
		assertEquals(runBehavior,builder.getRunBehavior());
		
	}

	@Test
	public void testReproductionBehavior() {
		assertNull(builder.getReproductionBehavior());
		
		builder.reproductionBehavior(reproductionBehavior);
		assertEquals(reproductionBehavior,builder.getReproductionBehavior());
	}

	@Test
	public void testBuild() {		
		builder.i(2)
		.j(2)
		.height(3)
		.width(5)
		.originalGroupID(10)
		.world(world)
		.genotype(genotype)
		.phenotype(phenotype)
		.moveBehavior(moveBehavior)
		.liveBehavior(liveBehavior)
		.runBehavior(runBehavior)
		.reproductionBehavior(reproductionBehavior);
		
		Genet genet = builder.build();
		
		assertEquals(builder.getI(), genet.getI());
		assertEquals(builder.getJ(),genet.getJ());
		assertEquals(builder.getHeight(),genet.getHeigth());
		assertEquals(builder.getWidth(),genet.getWidth());
		assertEquals(builder.getOriginalGroupID(),genet.getGroupID());
		assertEquals(builder.getWorld(),genet.getWorld());
		assertEquals(builder.getGenotype(),genet.getGenotype());
		assertEquals(builder.getPhenotype(),genet.getPhenotype());
		assertEquals(builder.getMoveBehavior(),genet.getMoveBehavior());
		assertEquals(builder.getLiveBehavior(),genet.getLiveBehavior());
		assertEquals(builder.getRunBehavior(),genet.getRunBehavior());
		assertEquals(builder.getReproductionBehavior(),genet.getReproductionBehavior());
	}

	@Test
	public void testLoadDocument() throws ParserConfigurationException, SAXException, IOException {
		String document = 
				"<GameOfLife>"
				+ "<Common>"
					+ "<PosX>12</PosX>"
					+ "<PosY>5</PosY>"
					+ "<Width>2</Width>"
					+ "<Height>3</Height>"
					+ "<OriginalGroupID>0</OriginalGroupID>"
				+ "</Common>"
				+ "<WorldSpecific>"
					+ "<World id=\"0\">"
						+ "<Genotype>"
							+ "<SexGen>X,Y</SexGen>"
							+ "<LifeSpanGen>71,19</LifeSpanGen>"
							+ "<SkinColorGen>174,44</SkinColorGen>"
							+ "<SpeedGen>2,1</SpeedGen>"
							+ "<TerrainAffinityGen>P,W</TerrainAffinityGen>"
						+ "</Genotype>"
						+ "<Phenotype>BiologicalAccuratePhenotype</Phenotype>"
						+ "<Behavior>"
							+ "<MoveBehavior>DefaultMoveBehavior</MoveBehavior>"
							+ "<LiveBehavior>DefaultLiveBehavior</LiveBehavior>"
							+ "<RunBehavior>DefaultMaleRunBehavior</RunBehavior>"
							+ "<ReproductionBehavior>DefaultReproductionBehavior</ReproductionBehavior>"
						+ "</Behavior>"
					+ "</World>"
				+ "</WorldSpecific>"
			+ "</GameOfLife>";
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
	    is.setCharacterStream(new StringReader(document));
	    
	    Document doc = db.parse(is);

		
		builder.loadDocument(doc);
		
		assertEquals(12, builder.getI());
		assertEquals(5, builder.getJ());
		assertEquals(3, builder.getHeight());
		assertEquals(2, builder.getWidth());
		assertEquals(0, builder.getOriginalGroupID());
		assertEquals("X,Y",builder.getGenotype().getSexGen().getGen());
		assertEquals("71,19",builder.getGenotype().getLifeSpanGen().getGen());
		assertEquals("174,44",builder.getGenotype().getSkinColorGen().getGen());
		assertEquals("2,1",builder.getGenotype().getSpeedGen().getGen());
		assertEquals("P,W",builder.getGenotype().getTerrainAffinityGen().getGen());
		assertEquals(BiologicalAccuratePhenotype.class, builder.getPhenotype().getClass());
		assertEquals(DefaultMoveBehavior.class, builder.getMoveBehavior().getClass());
		assertEquals(DefaultLiveBehavior.class, builder.getLiveBehavior().getClass());
		assertEquals(DefaultMaleRunBehavior.class, builder.getRunBehavior().getClass());
		assertEquals(DefaultReproductionBehavior.class, builder.getReproductionBehavior().getClass());
		
		
	}

}
