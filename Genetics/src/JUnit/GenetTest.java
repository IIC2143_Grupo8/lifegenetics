package JUnit;

import static org.junit.Assert.*;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.junit.After;
import org.junit.Test;

import lifegenetics.core.*;
import lifegenetics.views.*;
import lifegenetics.core.Genet.GenetBuilder;
import lifegenetics.core.Genotype.GenotypeBuilder;


public class GenetTest {


	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void GenetTest(){
		World world = new World(10, 10);
		

		
		world = new World(10, 10);
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				world.setTerrainAt(i, j, TerrainType.PLAIN);
		Random r = new Random();
		Obstacle obs = new Obstacle(1,1,1,1);
		world.setFabrica(obs);
		
		GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
		GenetBuilder genetBuilder = new GenetBuilder();
		genotypeBuilder.lifeSpanGen("100,100");
		genotypeBuilder.sexGen("X,X");
		genotypeBuilder.skinColorGen("255,255");
		genotypeBuilder.terrainAffinityGen("P,P");
		genotypeBuilder.speedGen("1,1");
		Genotype genotype = genotypeBuilder.build();
		Genet genet = genetBuilder.
				genotype(genotype).
				height(2).
				width(2).
				liveBehavior(new DefaultLiveBehavior()).
				moveBehavior(new DefaultMoveBehavior()).
				phenotype(new BiologicalAccuratePhenotype()).
				reproductionBehavior(new DefaultReproductionBehavior()).
				runBehavior(new DefaultFemaleRunBehavior()).
				world(world).
				build();
		

		

		world.enqueueInitialNewGenet(genet);
		int i = genet.getI();
		i = world.getTransformedI(i+1);
		int j = genet.getJ();
		j = world.getTransformedJ(j+1);
		
		genet.tryMove(i, j);
		world.setWorldElement(i,j,genet);
		
		
		assertEquals(genet, world.getWorld()[i][j]);
	}
	
	
	
	

}
