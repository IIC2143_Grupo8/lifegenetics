package JUnit;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import utils.WorldConnectionConfig;
import utils.WorldMetrics;

import lifegenetics.core.*;
import lifegenetics.core.Genet.GenetBuilder;
import lifegenetics.core.Genotype.GenotypeBuilder;
import lifegenetics.views.GameView;

public class WorldTest {

	World world1;
	GameConfiguration config;
	ArrayList<TerrainType> terrains;
	ArrayList<String> activeGens;
	WorldController worldController;

	@Before
	public void SetConfiguration() {
		terrains = new ArrayList<TerrainType> ();
		terrains.add(TerrainType.PLAIN);
		terrains.add(TerrainType.WATER);
		terrains.add(TerrainType.DESERT);
		
		activeGens = new ArrayList<String> ();
		activeGens.add("Vida");
		activeGens.add("Movimiento");
		activeGens.add("Afinidad con erreno");
		activeGens.add("Color");
		
		config = new GameConfiguration(20, 2, 1, terrains, activeGens);

		worldController = new WorldController(config);
		
		assertNull(world1);
		int tamanoDelMundo = config.getWorldSize();
		world1 = new World(tamanoDelMundo, tamanoDelMundo);
	}
	@Test
	public void configurationConditionTest()
	{
		assertEquals(config.getWorldSize(), 20);
		assertEquals(config.getTerrainTypes(), terrains);
		assertEquals(config.getActiveGens(), activeGens);
		assertEquals(config.getInitialGenets(), 2);
		assertEquals(config.getInitialObstacles(), 1);
	}
	
	@Test
	public void startSimulationTest()
	{
		assertNull(world1.getFabrica());
		world1.setFabrica(new Obstacle(8,8,1,1));
				
		assertEquals(world1.getFabrica(), world1.getWorld()[8][8]);
	}
	
	@Test
	public void isRunningTest()
	{
		world1.setRunning(true);		
		assertEquals(world1.isRunning(), true);
	}
	
	@Test
	public void heightTest()
	{
		world1.setHeight(21);		
		assertEquals(world1.getHeight(), 21);
	}
	
	@Test
	public void widthTest()
	{
		world1.setWidth(21);		
		assertEquals(world1.getWidth(), 21);
	}
	@Test
	public void genetAtTest()
	{
		Obstacle obs = new Obstacle(0,0,1,1);
		world1.setObstacle(obs);
		Genet g = world1.getGenetAt(0,0);
		assertNull(g);
	}
	@Test
	public void obstacleTest()
	{
		Obstacle obs = new Obstacle(0,0,1,1);
		world1.setObstacle(obs);		
		assertEquals(world1.getWorld()[0][0], obs);
	}

}
