package lifegenetics.core;

public interface MoveBehavior {
	void move(Genet genet);

}
