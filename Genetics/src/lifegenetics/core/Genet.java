package lifegenetics.core;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import lifegenetics.core.Genotype.GenotypeBuilder;
import lifegenetics.views.GameView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;


/**
 * Represent the alive entities of the simulation. It has a genotype, a phenotype and different behaviors.
 *
 */
public class Genet extends WorldElement implements Runnable	
{	
	
	/**
	 * Builder for genets
	 *
	 */
	public static class GenetBuilder
	{
		
		//Geters for JUnit tests
		public int getI() {
			return i;
		}

		public int getJ() {
			return j;
		}

		public int getHeight() {
			return height;
		}

		public int getWidth() {
			return width;
		}

		public int getOriginalGroupID() {
			return originalGroupID;
		}

		public World getWorld() {
			return world;
		}

		public Genotype getGenotype() {
			return genotype;
		}

		public Phenotype getPhenotype() {
			return phenotype;
		}

		public MoveBehavior getMoveBehavior() {
			return moveBehavior;
		}

		public LiveBehavior getLiveBehavior() {
			return liveBehavior;
		}

		public RunBehavior getRunBehavior() {
			return runBehavior;
		}

		public ReproductionBehavior getReproductionBehavior() {
			return reproductionBehavior;
		}

		public Document getDocument() {
			return document;
		}

		private int i;
		private int j;
		private int height;
		private int width;
		private int originalGroupID;
		private World world;
		private Genotype genotype;
		private Phenotype phenotype;
		private MoveBehavior moveBehavior;
		private LiveBehavior liveBehavior;
		private RunBehavior runBehavior;
		private ReproductionBehavior reproductionBehavior;
		private Document document;
		
		public GenetBuilder i(int i)
		{
			if(!(i<0))
			{
				this.i = i;	
			}
			return this;
		}
		
		public GenetBuilder j(int j)
		{
			if(!(j<0))
			{
				this.j = j;
			}
			return this;
		}
		
		public GenetBuilder height(int height)
		{
			if(!(height<1))
			{
				this.height = height;
			}
			return this;
		}
		
		public GenetBuilder width(int width)
		{
			if(!(width<1))
			{
				this.width = width;
			}
			return this;
		}
		
		public GenetBuilder originalGroupID(int originalGroupID)
		{
			this.originalGroupID = originalGroupID;
			return this;
		}
		
		public GenetBuilder world(World world)
		{
			this.world = world;
			return this;
		}
		
		public GenetBuilder genotype(Genotype genotype)
		{
			this.genotype = genotype;
			return this;
		}
		
		public GenetBuilder phenotype(Phenotype phenotype)
		{
			this.phenotype = phenotype;
			return this;
		}
		
		public GenetBuilder moveBehavior(MoveBehavior moveBehavior)
		{
			this.moveBehavior = moveBehavior;
			return this;
		}
		
		public GenetBuilder liveBehavior(LiveBehavior liveBehavior)
		{
			this.liveBehavior = liveBehavior;
			return this;
		}
		
		public GenetBuilder runBehavior(RunBehavior runBehavior)
		{
			this.runBehavior = runBehavior;
			return this;
		}
		
		public GenetBuilder reproductionBehavior(ReproductionBehavior reproductionBehavior)
		{
			this.reproductionBehavior = reproductionBehavior;
			return this;
		}
		
		public Genet build()
		{
			return new Genet(this);
		}
		
		/**
		 * Set the values of the builder from a DOM Document.
		 * @param document
		 * @return
		 */
		public GenetBuilder loadDocument(Document document)
		{
		    NodeList nodes = document.getElementsByTagName("Common");
		    Element common = (Element)nodes.item(0);
		    
		    NodeList node = common.getElementsByTagName("PosX");
		    Element line = (Element) node.item(0);
		    this.i = Integer.parseInt(line.getTextContent());
		    
		    node = common.getElementsByTagName("PosY");
		    line = (Element) node.item(0);
		    this.j = Integer.parseInt(line.getTextContent());
		    
		    node = common.getElementsByTagName("Width");
		    line = (Element) node.item(0);
		    this.width = Integer.parseInt(line.getTextContent());
		    
		    node = common.getElementsByTagName("Height");
		    line = (Element) node.item(0);
		    this.height = Integer.parseInt(line.getTextContent());
		    
		    node = common.getElementsByTagName("OriginalGroupID");
		    line = (Element) node.item(0);
		    this.originalGroupID = Integer.parseInt(line.getTextContent());
		    
		    
		    node = document.getElementsByTagName("World");
		    
		    Element world = null;
		    for(int i=0;i<node.getLength();i++)
		    {
		    	NamedNodeMap map = node.item(i).getAttributes();
		    	if(map.item(i).getTextContent().equals("0"))
		    	{
		    		world = (Element) node.item(i);
		    	}
		    }
		    if(world!=null)
		    {    
			    setFromWorld(node, line, world);
		    }
		    else
		    {
		    	Random r = new Random();
				String sexAlleles;
				String skinColorAlleles = "" + r.nextInt(255) + "," + r.nextInt(255) + "";
				String speedAlleles = "" + (r.nextInt(4) + 1) + "," + (r.nextInt(4) + 1) + "";
				String terrainAffinityAlleles = "";
				String lifeSpanAlleles = "" + (r.nextInt(49) + 50) + "," + (r.nextInt(49) + 1) + "";
				
				ArrayList<String> allelesGrouped = new ArrayList<String>();
				allelesGrouped.add(lifeSpanAlleles);
				allelesGrouped.add(terrainAffinityAlleles);
				allelesGrouped.add(speedAlleles);
				allelesGrouped.add(skinColorAlleles);

				int terrainAffinityNumber = 0;
				for (int i = 0; i < 2; i++)
				{
					terrainAffinityNumber = r.nextInt(3);
					if(terrainAffinityNumber == 0)
					{
						terrainAffinityAlleles += "W";
					}
					else if(terrainAffinityNumber == 1)
					{
						terrainAffinityAlleles += "D";
					}
					else
					{
						terrainAffinityAlleles += "P";
					}
					
					if(i==0)
					{
						terrainAffinityAlleles += ",";
					}
				}
				
				GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
				
				ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();
				genotypeBuilder = buildNewGenotype(initialGens, genotypeBuilder, allelesGrouped);
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					this.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					this.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				Genotype genotype = genotypeBuilder.build();
								
				this.
					genotype(genotype).
					liveBehavior(new DefaultLiveBehavior()).
					moveBehavior(new DefaultMoveBehavior()).
					phenotype(new BiologicalAccuratePhenotype()).
					reproductionBehavior(new DefaultReproductionBehavior());
		    }
		    
		    this.document = document;
		    
			return this;
		}
		protected GenotypeBuilder buildNewGenotype(ArrayList<String> initialGens, GenotypeBuilder genotypeBuilder, ArrayList<String> allelesGrouped)
		{
			if(initialGens.contains("Vida"))
			{
				genotypeBuilder.lifeSpanGen(allelesGrouped.get(0));
			}
			if(initialGens.contains("Movimiento"))
			{
				genotypeBuilder.speedGen(allelesGrouped.get(1));
			}
			if(initialGens.contains("Afinidad con terreno"))
			{
				genotypeBuilder.terrainAffinityGen(allelesGrouped.get(2));
			}
			if(initialGens.contains("Color"))
			{
				genotypeBuilder.skinColorGen(allelesGrouped.get(3));
			}
			return genotypeBuilder;
		}
		protected void setFromWorld(NodeList node, Element line, Element world)
		{
			node = world.getElementsByTagName("Phenotype");
		    line = (Element) node.item(0);

		    switch(line.getTextContent())
		    {
			    case("BiologicalAccuratePhenotype"):
			    	this.phenotype = new BiologicalAccuratePhenotype();
			    	break;
			    default:
			    	this.phenotype = new BiologicalAccuratePhenotype();
			    	break;
		    }
		    
		    node  = world.getElementsByTagName("Behavior");
		    Element behavior = (Element) node.item(0);
		    
		    node = behavior.getElementsByTagName("MoveBehavior");
		    line = (Element) node.item(0);
		    
		    switch(line.getTextContent())
		    {
			    case("DefaultMoveBehavior"):
			    	this.moveBehavior = new DefaultMoveBehavior();
			    	break;
			    default:
			    	this.moveBehavior = new DefaultMoveBehavior();
			    	break;
		    }
		    
		    node = behavior.getElementsByTagName("LiveBehavior");
		    line = (Element) node.item(0);
		    
		    switch(line.getTextContent())
		    {
			    case("LiveBehavior"):
			    	this.liveBehavior = new DefaultLiveBehavior();
			    	break;
			    default:
			    	this.liveBehavior = new DefaultLiveBehavior();
			    	break;
		    }
		    
		    node = behavior.getElementsByTagName("ReproductionBehavior");
		    line = (Element) node.item(0);
		    
		    switch(line.getTextContent())
		    {
			    case("ReproductionBehavior"):
			    	this.reproductionBehavior = new DefaultReproductionBehavior();
			    	break;
			    default:
			    	this.reproductionBehavior = new DefaultReproductionBehavior();
			    	break;
		    }
		    
		    node = behavior.getElementsByTagName("RunBehavior");
		    line = (Element) node.item(0);
		    
		    switch(line.getTextContent())
		    {
			    case("DefaultFemaleRunBehavior"):
			    	this.runBehavior = new DefaultFemaleRunBehavior();
			    	break;
			    case("DefaultMaleRunBehavior"):
			    	this.runBehavior = new DefaultMaleRunBehavior();
			    	break;
			    default:
			    	this.runBehavior = new DefaultFemaleRunBehavior();
			    	break;
		    }
		    
		    
		    node  = world.getElementsByTagName("Genotype");
		    Element genotypeNode = (Element) node.item(0);
		    
		    
		    GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
		    
		    node = genotypeNode.getElementsByTagName("SexGen");
		    line = (Element) node.item(0);
		    genotypeBuilder.sexGen(line.getTextContent());
		    
		    node = genotypeNode.getElementsByTagName("LifeSpanGen");
		    line = (Element) node.item(0);
		    genotypeBuilder.lifeSpanGen(line.getTextContent());
		    
		    node = genotypeNode.getElementsByTagName("SkinColorGen");
		    line = (Element) node.item(0);
		    genotypeBuilder.skinColorGen(line.getTextContent());
		    
		    node = genotypeNode.getElementsByTagName("SpeedGen");
		    line = (Element) node.item(0);
		    genotypeBuilder.speedGen(line.getTextContent());
		    
		    node = genotypeNode.getElementsByTagName("TerrainAffinityGen");
		    line = (Element) node.item(0);
		    genotypeBuilder.terrainAffinityGen(line.getTextContent());
		    
		    this.genotype = genotypeBuilder.build();
		}
	}
	
	private Genet(GenetBuilder builder)
	{
		super(builder.i,builder.j,builder.height,builder.width);
		
		this.world = builder.world;
		this.genotype = builder.genotype;
		this.phenotype =  builder.phenotype;
		this.moveBehavior =  builder.moveBehavior;
		this.liveBehavior = builder.liveBehavior;
		this.runBehavior = builder.runBehavior;
		this.reproductionBehavior = builder.reproductionBehavior;
		this.groupID = builder.originalGroupID;
		this.document = builder.document;
		setMoveTrend("");
	}
	
	private World world;
	private Genotype genotype;
	private Phenotype phenotype;
	
	private MoveBehavior moveBehavior;
	private LiveBehavior liveBehavior;
	private RunBehavior runBehavior;
	private ReproductionBehavior reproductionBehavior;
	
	private String moveTrend;
	
	private int groupID;
	
	private Document document;
	
	private int reproductiveCooldown = 0;
	
	// This variable represent the life of the Genet in terms of turns of the simulation
	private int actualLife = 0;
	
	// This variable represents the turns you have to wait to move the genet 
	private int waitToMove = 0;
	
 	
 	public int getSpeed()
 	{
 		return phenotype.getSpeed(genotype,world.getEnvironment(this));
 	}
 	
 	public Sex getSex()
 	{
 		return phenotype.getSex(genotype);
 	}
 	
 	public int getLifeSpan()
 	{
 		return phenotype.getLifeSpan(genotype);
 	}
 	
 	public int getSkinColor()
 	{
 		return phenotype.getSkinColor(genotype, world.getEnvironment(this));
 	}
 	
 	public TerrainType getTerrainAffinity()
 	{
 		return phenotype.getTerrainAffinity(genotype);
 	}
 	
 	public Genotype getGenotype()
 	{
 		return genotype;
 	}
 	
	public Phenotype getPhenotype() {
		return phenotype;
	}
 	
	public MoveBehavior getMoveBehavior() {
		return moveBehavior;
	}

	public LiveBehavior getLiveBehavior() {
		return liveBehavior;
	}

	public RunBehavior getRunBehavior() {
		return runBehavior;
	}

	public ReproductionBehavior getReproductionBehavior() {
		return reproductionBehavior;
	}
	
 	public int getActualLife()
 	{
 		return this.actualLife;
 	}
 	
 	public int getGroupID()
 	{
 		return groupID;
 	}
 	
 	public World getWorld()
 	{
 		return world;
 	}
 	
 	public void incrementActualLife()
 	{
 		 actualLife++;
 	}
 	
 	public void decrementReproductiveCooldown()
 	{
 		reproductiveCooldown--;
 	}
 	
 	public boolean isInReproductiveCooldown()
 	{
 		return reproductiveCooldown > 0;
 	}
 	
 	public int getWaitToMove()
 	{
 		return waitToMove;
 	}
 	
 	public void setWaitToMove(int wait)
 	{
 		waitToMove = wait;
 	}
	
  	public void run()	
 	{
  		while(isAlive()){
  			if(world.isRunning()){
  				runBehavior.run(this);
  	 			try {
  	 				Thread.sleep((long)(500/world.getTimeMultiplier()));
  	 			} catch (InterruptedException e) {
  	 				// TODO Auto-generated catch block
  	 				e.printStackTrace();
  	 			}
  			} 			
 		}
 	}
 	
 	/**Search for a genet in the inmediate neighbourhood to reproduce.
 	 * 
 	 */
 	public void findSexMate()
 	{
 		//System.out.println("Buscando pareja");
 		world.findSexMate(this);
 	}
 	
 	
 	/**
 	 * Creates a new genet using the rules defined in the reproductionBehavior.
 	 * If the genet is in reproductive cooldown then it does nothing.
 	 * @param genet The other genet .
 	 */
 	public void reproduce(Genet genet)
 	{
		if(phenotype.getSex(genotype) == Sex.FEMALE){
 			reproductiveCooldown = 10;
		}
 		System.out.println("Pareja encontrada");
 		Genotype childGenotype =reproductionBehavior.reproduce(this, genet);
 		Genet child;
 		
 		GenetBuilder genetBuilder = new GenetBuilder();
 		if(phenotype.getSex(childGenotype) == Sex.FEMALE)
 		{
 			genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
 		}
 		else
 		{
 			genetBuilder.runBehavior(new DefaultMaleRunBehavior());
 		}
		child = genetBuilder.
				height(height).
				width(width).
				genotype(childGenotype).
				phenotype(phenotype).
				liveBehavior(liveBehavior).
				moveBehavior(moveBehavior).
				reproductionBehavior(reproductionBehavior).
				runBehavior(runBehavior).
				world(world).
				build();	
 		world.enqueueNewGenet(child);
 	}
 	

 	/**
 	 * Tries to move the genet verticalDisplacement units vertically and horizontalDisplacement horizontally.
 	 * If there is no room for the genet in the new place then it doesn't move.
 	 * @param verticalDisplacement
 	 * @param horizontalDisplacement
 	 */
 	public boolean tryMove(int verticalDisplacement, int horizontalDisplacement)
 	{
 		return world.tryMove(this, verticalDisplacement, horizontalDisplacement);
 	}
 	
 	
 	/**
 	 * Removes the genet from the world
 	 */
 	public void die()
 	{
 		world.removeGenet(this);
 		actualLife = getLifeSpan();
 	}
 	
 	public boolean isAlive()
 	{
 		return actualLife<getLifeSpan();
 	}
 	
 	/** Serializes the genet into XML.
 	 * If the genet document is null, then it stores the generated XML into the document, 
 	 * else updates the document with the actual data.
 	 * @return an XML representation of the genet.
 	 * @throws ParserConfigurationException
 	 */
 	public String serialize() throws ParserConfigurationException
 	{
 		if(document==null)
 		{
 			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument();
			
			Element rootElement = doc.createElement("GameOfLife");
			doc.appendChild(rootElement);
			
			Element common = doc.createElement("Common");
			rootElement.appendChild(common);
			
			Element posXN = doc.createElement("PosX");
			posXN.appendChild(doc.createTextNode(this.i+""));
			common.appendChild(posXN);
			
			Element posYN = doc.createElement("PosY");
			posYN.appendChild(doc.createTextNode(this.j+""));
			common.appendChild(posYN);
			
			Element widthN = doc.createElement("Width");
			widthN.appendChild(doc.createTextNode(this.width+""));
			common.appendChild(widthN);
			
			Element heightN = doc.createElement("Height");
			heightN.appendChild(doc.createTextNode(this.height+""));
			common.appendChild(heightN);
			
			Element originalN = doc.createElement("OriginalGroupID");
			originalN.appendChild(doc.createTextNode("0"));
			common.appendChild(originalN);
			
			
			Element worldSpecific = doc.createElement("WorldSpecific");
			rootElement.appendChild(worldSpecific);
			
			Element worldN = doc.createElement("World");
			worldN.setAttribute("id", "0");
			worldSpecific.appendChild(worldN);
			
			Element genotypeN = doc.createElement("Genotype");
			worldN.appendChild(genotypeN);
			
			Element sexGen = doc.createElement("SexGen");
			sexGen.appendChild(doc.createTextNode(genotype.getSexGen().getGen()));
			genotypeN.appendChild(sexGen);
			
			Element lifeSpanGen = doc.createElement("LifeSpanGen");
			lifeSpanGen.appendChild(doc.createTextNode(genotype.getLifeSpanGen().getGen()));
			genotypeN.appendChild(lifeSpanGen);
			
			Element skinColorGen = doc.createElement("SkinColorGen");
			skinColorGen.appendChild(doc.createTextNode(genotype.getSkinColorGen().getGen()));
			genotypeN.appendChild(skinColorGen);
			
			Element speedGen = doc.createElement("SpeedGen");
			speedGen.appendChild(doc.createTextNode(genotype.getSpeedGen().getGen()));
			genotypeN.appendChild(speedGen);
			
			Element terrainAffinityGen = doc.createElement("TerrainAffinityGen");
			terrainAffinityGen.appendChild(doc.createTextNode(genotype.getTerrainAffinityGen().getGen()));
			genotypeN.appendChild(terrainAffinityGen);
			
			Element phenotypeN = doc.createElement("Phenotype");
			phenotypeN.appendChild(doc.createTextNode(phenotype.toString()));
			worldN.appendChild(phenotypeN);
			
			Element behavior = doc.createElement("Behavior");
			worldN.appendChild(behavior);
			
			Element moveBehaviorN = doc.createElement("MoveBehavior");
			moveBehaviorN.appendChild(doc.createTextNode(moveBehavior.toString()));
			behavior.appendChild(moveBehaviorN);
			
			Element liveBehaviorN = doc.createElement("LiveBehavior");
			liveBehaviorN.appendChild(doc.createTextNode(liveBehavior.toString()));
			behavior.appendChild(liveBehaviorN);
			
			Element runBehaviorN = doc.createElement("RunBehavior");
			runBehaviorN.appendChild(doc.createTextNode(runBehavior.toString()));
			behavior.appendChild(runBehaviorN);
			
			Element reproductionBehaviorN = doc.createElement("ReproductionBehavior");
			reproductionBehaviorN.appendChild(doc.createTextNode(reproductionBehavior.toString()));
			behavior.appendChild(reproductionBehaviorN);
			
			
			this.document = doc;
 		}
 		else
 		{
 			NodeList list =document.getElementsByTagName("PosX");
 			list.item(0).setTextContent(this.i+"");
 			
 			list =document.getElementsByTagName("PosY");
 			list.item(0).setTextContent(this.j+"");
 			
 			list =document.getElementsByTagName("Width");
 			list.item(0).setTextContent(this.width+"");
 			
 			list =document.getElementsByTagName("Height");
 			list.item(0).setTextContent(this.height+"");
 		}
			DOMSource source = new DOMSource(document);
						 
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer;
			
			StringWriter sw = new StringWriter();
			
			try {	
				transformer = tf.newTransformer();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				transformer.transform(source, new StreamResult(sw));
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
			// En sw queda el string, a consola para pruebas 
			
			System.out.println(sw.toString());
			return sw.toString();
 	}

	public String getMoveTrend() {
		return moveTrend;
	}

	public void setMoveTrend(String moveTrend) {
		this.moveTrend = moveTrend;
	}
}