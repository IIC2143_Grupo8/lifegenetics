package lifegenetics.core;

/**
 * Collection of gens.
 *
 */
public class Genotype {
	
	public static class GenotypeBuilder 
	{
		private SexGen sexGen;
		private LifeSpanGen lifeSpanGen;
		private SkinColorGen skinColorGen;
		private SpeedGen speedGen;
		private TerrainAffinityGen terrainAffinityGen;
		
		public GenotypeBuilder sexGen(String sexGen)
		{
			this.sexGen = new SexGen(sexGen);
			return this;
		}
		
		public GenotypeBuilder lifeSpanGen(String lifeSpanGen)
		{
			this.lifeSpanGen = new LifeSpanGen(lifeSpanGen);
			return this;
		}
		
		public GenotypeBuilder skinColorGen(String skinColorGen)
		{
			this.skinColorGen = new SkinColorGen(skinColorGen);
			return this;
		}
		
		public GenotypeBuilder speedGen(String speedGen)
		{
			this.speedGen = new SpeedGen(speedGen);
			return this;
		}
		
		public GenotypeBuilder terrainAffinityGen(String terrainAffinityGen)
		{
			this.terrainAffinityGen = new TerrainAffinityGen(terrainAffinityGen);
			return this;
		}
		
		public Genotype build()
		{
			if(skinColorGen==null)
			{
				this.skinColorGen = new SkinColorGen("100,100");
			}
			if(terrainAffinityGen==null)
			{
				this.terrainAffinityGen = new TerrainAffinityGen("P,P");
			}
			if(lifeSpanGen==null)
			{
				this.lifeSpanGen = new LifeSpanGen("75,75");
			}
			if(speedGen==null)
			{
				this.speedGen = new SpeedGen("2,2");
			}
			
			return new Genotype(this);
		}
	}
	
	Gen[] genotype;
	
	/*
	 * 0: Sex
	 * 1: LifeSpan
	 * 2: SkinColor
	 * 3: Speed
	 * 4: TerrainAffinity
	 */
	private Genotype(GenotypeBuilder builder)
	{
		genotype = new Gen[5];
		genotype[0] = builder.sexGen;
		genotype[1] = builder.lifeSpanGen;
		genotype[2] = builder.skinColorGen;
		genotype[3] = builder.speedGen;
		genotype[4] = builder.terrainAffinityGen;
	}
	
	public Gen getSexGen()
	{
		return genotype[0];
	}
	
	public Gen getLifeSpanGen()
	{
		return genotype[1];
	}
	
	public Gen getSkinColorGen()
	{
		return genotype[2];
	}
	
	public Gen getSpeedGen()
	{
		return genotype[3];
	}
	
	public Gen getTerrainAffinityGen()
	{
		return genotype[4];
	}
	
	public Gen[] getGenArray()
	{
		return genotype;
	}
	
	public String toString()
	{
		String output="[";
		for(int i=0;i<genotype.length;i++)
		{
			output+="|"+genotype[i].getGen();
		}
		output+="|]";
		return output;
	}
}
