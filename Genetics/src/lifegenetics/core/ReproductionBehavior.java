package lifegenetics.core;

public interface ReproductionBehavior {
	Genotype reproduce(Genet mother,Genet father);
}
