package lifegenetics.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.ParserConfigurationException;

import lifegenetics.views.GameView;
import utils.WorldConnectionConfig;
import utils.WorldMetrics;


public class WorldController {
	private World world;
	private GameConfiguration config;
	private int timeCounter;
    private Timer timer;
    private double timeMultiplier;
    private static int CoinsPerRound = 2;

	public void setTimeMultipler(double multiplier)
	{
		world.setTimeMultiplier(multiplier);
	}
	
	public double getTimeMultiplier(){
		return timeMultiplier;
	}
	public void killGenet(Genet genet)
	{
		genet.die();
	}
    
	public WorldController(GameConfiguration config)
	{
		this.config = config;
		timeMultiplier = 1;
	}
	
	/**
	* La SimulaciÃ³n es pausada por el usuario
	*/
	public void stopSimulation()
	{
		world.setRunning(false);
	}
	
	/**
	* El usuario elije continuar la partida.
	*/
	public void continueSimulation()
	{
		world.setRunning(true);
	}
	
	/**
	* Se expande el mundo
	* @param newSize Nuevo tamaÃ±o del mundo.
	* @param gameView el Gameview en uso
	*/
	public TerrainType[][] expandWorld(int newSize)
	{
		int originalSize = world.getHeight();
		TerrainType[][] newTerrain = new TerrainType[newSize][newSize];
		WorldElement[][] newWorld = new WorldElement[newSize][newSize];
		world.setHeight(newSize);
		world.setWidth(newSize);
		config.setWorldSize(newSize);
		for(int i = 0; i < newSize; i++){
			for(int j = 0; j < newSize; j++)
			{
				if(i < originalSize && j < originalSize){
					newTerrain[i][j] = world.getTerrain()[i][j];
					newWorld[i][j] = world.getWorld()[i][j];
					//gameView.setTerrainOnCoords(i, j, world.getTerrain()[i][j]);
				}
				else
				{
					newTerrain[i][j] = TerrainType.PLAIN;
					//gameView.setTerrainOnCoords(i, j, newTerrain[i][j]);
					newWorld[i][j] = null;
				}
			}
		}

		
		world.setTerrain(newTerrain);
		world.setWorld(newWorld);
		return newTerrain;
		//for (int i = 0; i < newSize; i++)
			//for (int j = 0; j < newSize; j++)
				//gameView.setTerrainOnCoords(i, j, world.getTerrain()[i][j]);
	}
	/** This method modifies a field of the world terrain
	 *  according to the position and the new terrain
	 *  @param x : position x of the field to modify
	 *  @param y : position y of the field to modify
	 *  @param field : the new terrain of field to modify
	 * */
	public void modifyTerrain(int x,int y, TerrainType field, GameView gameView)
	{
		world.setField(x, y, field);
		gameView.setTerrainOnCoords(x, y, field);
	}
	
	public void startSimulation(GameView gameView)
	{
		int tamanoDelMundo = config.getWorldSize();
		world = new World(tamanoDelMundo, tamanoDelMundo);
		world.addObserver(gameView);
		gameView.setObservable(world);
		generateTerrain();
		TerrainType[][] terrain = world.getTerrain();
		for (int x = 0; x < tamanoDelMundo; x++)
			for (int y = 0; y < tamanoDelMundo; y++)
				gameView.setTerrainOnCoords(x, y, terrain[x][y]);
		int initialGenets = config.getInitialGenets();
		Random r = new Random();
		world.setFabrica(new Obstacle(r.nextInt(tamanoDelMundo), r.nextInt(tamanoDelMundo),1,1));
		
		int initialObstacles = config.getInitialObstacles();
		for(int obstacleCounter = 0; obstacleCounter < initialObstacles; obstacleCounter++)
		{
			Random r2 = new Random();
			world.setObstacle(new Obstacle(r2.nextInt(tamanoDelMundo), r2.nextInt(tamanoDelMundo),1,1));
		}
		
		for(int genetCounter = 0; genetCounter < initialGenets; genetCounter++)
		{
			world.createRandomGenet(r);
		}
		
		world.addInitialGenets(initialGenets, r);
		

		timer = new Timer();
		timer.schedule(new TimerTask() {
			  @Override
			  public void run() {
				  if(world.isRunning()){
					  timeCounter += 1;
					  world.setCoins(world.getCoins() + CoinsPerRound);
				  }
			  }
		}, (long)(500/timeMultiplier), (long)(500/timeMultiplier));
		
		


	}
	
	/**
	 * Fills the terrain with the terrainTypes specified by the user,
	 * using the method "world.setTerrain()"
	 * 
	 */
	public void generateTerrain()
	{
		ArrayList<TerrainType> terrains = config.getTerrainTypes();
		boolean hasPlain = false;
		boolean hasDesert = false;
		boolean hasWater = false;
		if(terrains.contains(TerrainType.PLAIN))
		{
			hasPlain = true;
		}
		if(terrains.contains(TerrainType.DESERT))
		{
			hasDesert = true;
		}
		if(terrains.contains(TerrainType.WATER))
		{
			hasWater = true;
		}
		world.setInitialTerrain(hasDesert, hasWater, hasPlain);
	}

	public int getCoins()
	{
		return world.getCoins();
	}
	
	/**
	 * Returns the names of the available suggested Genets
	 * @return A list of names.
	 */
	public List<String> GenetsSugeridos()
	{
		List<String> lista = new ArrayList<String>();
		lista.add("Genet aleatorio");
		// lista.add("Genet de llanura");
		lista.add("Genet de agua");
		lista.add("Genet de desierto");
		lista.add("Genet duradero y fuerte");
		lista.add("Genet monstruo");
		// lista.add("Genet fantasma");
		lista.add("Genet mutante");
		lista.add("Genet longevo");
		lista.add("Genet fugaz");

		return lista;
	}
	
	/**
	 * Creates a genet from the index of the available suggested Genets. 
	 * @param index This is how you identificate the Genet from the sent list.
	 * @return True if the operation was completed. False otherwise. 
	 */
	public boolean createGenetByCoin(int index)
	{
		return world.createGenetByCoins(index);
	}
	
	public boolean createGenetByCoin()
	{
		return world.createGenetByCoins();
	}

	public Genet getGenetAt(int i, int j) {
		// TODO Auto-generated method stub
		return world.getGenetAt(i, j);
	}

	public TerrainType getTerrainAt(int i, int j) {
		return world.getTerrainAt(i, j);
	}
	public void setTerrainAt(int i, int j, TerrainType newterrain) {
		world.setTerrainAt(i, j, newterrain);
	}

	/**
	 * Se manda el genet seleccionado a otro mundo
	 * @param genet Genet a viajar
	 * @param destinationIndex Ã�ndice del mundo donde llegarÃ¡ el genet.
	 * @see mÃ©todo: public List<String> getListaDeMundos()
	 */
	public void migrateGenet(Genet genet, int destinationIndex) {
		Socket socket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        WorldConnectionConfig config = world.getWorldsConfig().get(destinationIndex);      
 
        String ipList[] = config.getIp();
        int port = config.getPort();
        for(int i=0;i<ipList.length;i++)
        {
	        try {
				socket = new Socket(ipList[i],port);
	           
	            out = new PrintWriter(socket.getOutputStream(), true);
	            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	        } catch (UnknownHostException e) {
	            System.err.println("Don't know about host");
	            System.exit(1);
	        } catch (IOException e) {
	            System.err.println("Couldn't get I/O for the connection");
	            System.exit(1);
	        }
	        
	        String xml;
			try {
				xml = genet.serialize();
				out.println(xml);
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        out.close();
	        try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        genet.die();
		
	}
	
	public List<String> getListaDeMundos()
	{
		List<String> lista = new ArrayList<String>();
		for(WorldConnectionConfig config : world.getWorldsConfig())
		{
			lista.add(config.getName());
		}
		
		return lista;	
	}
	
	public WorldMetrics getInitialMetrics()
	{
		return world.getInitialMetrics();
	}
	
	public WorldMetrics getActualMetrics()
	{
		return world.getActualMetrics();
	}
	
	public int getActualGenetsCount()
	{
		return world.getActualGenetsCount();
	}

}
