package lifegenetics.core;
import java.util.Observable;


public abstract class WorldElement extends Observable{
	protected int width;
	protected int height;
	protected int i;
	protected int j;
	
	public WorldElement(int i,int j, int height, int width)
	{
		this.i = i;
		this.j = j;
		this.height = height;
		this.width = width;
	}
	
	public int getWidth()
	{
		return width;	
	}
	
	public void setWidth(int width)
	{
		this.width = width;
	}
	
	public int getHeigth()
	{
		return height;
	}
	
	public void setHeigth(int heigth)
	{
		this.height = heigth; 
	}
	
	public int getI()
	{
		return i;
	}
	
	public void setI(int i)
	{
		this.i=i;
	}
	
	public int getJ()
	{
		return j;
	}
	
	public void setJ(int j)
	{
		this.j=j;
	}

}
