package lifegenetics.core;

public interface LiveBehavior {
	void adjustHealth(Genet genet);
}
