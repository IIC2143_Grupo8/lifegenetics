package lifegenetics.core;

public class DefaultReproductionBehavior implements ReproductionBehavior{

	@Override
	public Genotype reproduce(Genet mother,Genet father) {
		Gen[] motherGenotype = mother.getGenotype().getGenArray();
		Gen[] fatherGenotype = father.getGenotype().getGenArray();
		
		String[] childGenotype = new String[fatherGenotype.length];
		for(int i=0;i<motherGenotype.length;i++)
		{
			String[] motherAllele = motherGenotype[i].getGen().split(",");
			String[] fatherAllele = fatherGenotype[i].getGen().split(",");
			
			int selectedMother = (int)(Math.random()*2);
			int selectedFather = (int)(Math.random()*2);
			
			childGenotype[i]=motherAllele[selectedMother]+","+fatherAllele[selectedFather];
		}
		
		
		/*
		 * 0: Sex
		 * 1: LifeSpan
		 * 2: SkinColor
		 * 3: Speed
		 * 4: TerrainAffinity
		 */
		
		Genotype child = new Genotype.GenotypeBuilder().sexGen(childGenotype[0]).lifeSpanGen(childGenotype[1]).skinColorGen(childGenotype[2]).speedGen(childGenotype[3]).terrainAffinityGen(childGenotype[4]).build();
		return child;
	}
	
	@Override public String toString()
	{
		return "DefaultReproductionBehavior";
	}

}
