package lifegenetics.core;

public abstract class Gen {
	private String gen;
	
	/**
	 * @return The string representation of the gen
	 */
	public String getGen()
	{
		return gen;
	}
	
	/** Represents a gen with two alleles
	 * @param gen the two alleles separated by a "," (i.e, "125,300")
	 */
	public Gen(String gen)
	{
		this.gen = gen;
	}
	
	public String toString()
	{
		String[] splitted = gen.split(",");
		return  "["+splitted[0]+"|"+splitted[1]+"]";
	}
}