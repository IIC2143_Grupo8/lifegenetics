package lifegenetics.core;

public class DefaultLiveBehavior implements LiveBehavior{

	@Override
	public void adjustHealth(Genet genet) {
		// TODO Auto-generated method stub
		genet.incrementActualLife();
		
		if(genet.getActualLife() >= genet.getLifeSpan())
		{
			genet.die();
		}
		
	}
	
	@Override public String toString()
	{
		return "DefaultLiveBehavior";
	}

}
