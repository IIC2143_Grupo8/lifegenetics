package lifegenetics.core;

/**
 * Storages the speed gen of a genet. 
 * The speed represents the number of turns that a genet has to wait
 * before moving so lower values mean faster genets.
 *
 */
public class SpeedGen extends Gen{

	public SpeedGen(String gen) {
		super(gen);
		// TODO Auto-generated constructor stub
	}

}
