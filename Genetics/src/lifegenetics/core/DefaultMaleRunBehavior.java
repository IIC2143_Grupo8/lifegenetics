package lifegenetics.core;

public class DefaultMaleRunBehavior implements RunBehavior{

	@Override
	public void run(Genet genet) {
 		genet.getMoveBehavior().move(genet);
 		genet.getLiveBehavior().adjustHealth(genet);
		genet.findSexMate();
	}
	
	@Override public String toString()
	{
		return "DefaultMaleRunBehavior";
	}
}
