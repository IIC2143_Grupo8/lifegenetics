package lifegenetics.core;

/**
 * Gen that storages the expect life span of a genet
 *
 */
public class LifeSpanGen extends Gen{

	public LifeSpanGen(String gen) {
		super(gen);
	}

}
