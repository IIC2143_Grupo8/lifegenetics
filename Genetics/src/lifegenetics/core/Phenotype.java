package lifegenetics.core;

public abstract class Phenotype {
	
	/**
	 * @param genotype of the calling Genet
	 * @param environment of the calling Genet
	 * @return the speed in cell numbers of the calling Genet
	 */
	
	abstract int getSpeed(Genotype genotype,TerrainType environment);
	
	/**
	 * @param genotype of the calling Genet
	 * @return the type of terrain that the calling Genet y Affine to
	 */
	abstract TerrainType getTerrainAffinity(Genotype genotype);
	
	
	/**
	 * @param genotype of the calling Genet
	 * @return the sex of the calling Genet
	 */
	abstract Sex getSex(Genotype genotype);
	
	
	/**
	 * @param genotype of the calling Genet
	 * @return the lifeSpan in thicks of the calling Genet
	 */
	abstract int getLifeSpan(Genotype genotype);
	
	
	/**
	 * @param genotype of the calling Genet
	 * @param environment of the calling Genet
	 * @return the skin color in gray scale of the calling Genets
	 */
	abstract int getSkinColor(Genotype genotype, TerrainType environment);
	
}
