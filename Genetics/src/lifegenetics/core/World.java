package lifegenetics.core;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import lifegenetics.core.Genet.GenetBuilder;
import lifegenetics.core.Genotype.GenotypeBuilder;
import lifegenetics.views.GameOverDialog;
import lifegenetics.views.GameView;
import utils.WorldConnectionConfig;
import utils.WorldConnectionConfigLoader;
import utils.WorldMetrics;

import org.javatuples.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.javafx.font.Metrics;

// The Observer Pattern: We shall only notify the GameView about change of Genet in the following three situations:
// 1. Move
// 2. Newborn
// 3. Dead.
// We use setChanged(); notifyObservers(); in the corresponding sections.


public class World extends Observable{

	private WorldElement[][] world;
	private TerrainType[][] terrain;
	private int width;
	private int height;
	private Queue<Genet> waitingGenets;
	private int coins;
	private boolean running;
	private boolean listening;
	private Obstacle fabrica;
	private List<Obstacle> obstacles;
	private double timeMultiplier;
	
	private int actualGenets;
	
	private WorldMetrics initialMetrics;
	private WorldMetrics actualMetrics;
	
	private List<WorldConnectionConfig> worldsConfig;

	private Thread genetAdder=new Thread(new Runnable() {
	    public void run()
	    {
	    	addGenet();
	    }
	});
	
	private Thread worldsListener = new Thread(new Runnable() {
		public void run()
		{
			try {
				listenOtherWorlds();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
			}
		}
	});
	
    public void setWorldElement(int i, int j, WorldElement e)
    {
    	world[i][j] = e;
    }
	
	private void addGenet()
	{
		while(true)
		{
			synchronized (waitingGenets) {
				while(waitingGenets.isEmpty()){
					try {
						waitingGenets.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			Genet genet=waitingGenets.poll();
			boolean tryingToMove = true;
			while(tryingToMove)
			{
				genet.setI(0);
				genet.setJ(0);
				setInitialPositionForGenet(genet);
				tryingToMove = !tryMove(genet,0,0);
			}
			
			Thread t = new Thread(genet);
			t.start();
			actualGenets++;
			
			String text = "Nuevo genet ha nacido con genotipo " + genet.getGenotype();
			System.out.println(text);
			
			// Now notify GameView to update the changes in genets.
			// Here we only need to pass it the message that a Genet was born. No need to update.
			setChanged();
			notifyObservers(text);
		}
	}
	
	private void listenOtherWorlds() throws IOException, ParserConfigurationException, SAXException
	{
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(1234);
        } catch (IOException e) {
            serverSocket = new ServerSocket(0);
            System.err.println("Could not listen on port: 1234. Now listening in "+serverSocket.getLocalPort());
        }

		while(listening)
		{
	        Socket clientSocket = null;
	        try {
	            clientSocket = serverSocket.accept();
	        } catch (IOException e) {
	            System.err.println("Accept failed.");
	            System.exit(1);
	        }
	        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
	        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			String xml = in.readLine();
			
			System.out.println(xml);
			
			DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(xml));
		    
		    Document doc = db.parse(is);
		    
		    
		    GenetBuilder genetBuilder = new GenetBuilder();
		    genetBuilder.loadDocument(doc);
		    genetBuilder.i(0);
		    genetBuilder.j(0);
		    genetBuilder.world(this);
			
			Genet genet = genetBuilder.build();
			enqueueNewGenet(genet);

	        out.close();
	        in.close();
	        clientSocket.close();
		}
        serverSocket.close();
	}
	
	/**
	 * Removes the Genet that is set in the given position
	 * 
	 * @param i is the horizontal position
	 * @param j is the vertical position
	 */
	public void removeGenet(Genet genet)
	{
		
		ArrayList<Pair<Integer, Integer>> coordinates = new ArrayList<Pair<Integer, Integer>>();
		for(int i=0; i<genet.width; i++)
		{
			for(int j=0;j<genet.height;j++)
			{
				int I = getTransformedI(genet.i + i);
				int J = getTransformedJ(genet.j + j);
				coordinates.add(Pair.with(I, J));
				world[I][J] = null;
			}
		}
		
		setChanged();
		notifyObservers(coordinates);
		
		String text = "Murió un genet en la coordenada "+ genet.i + "," + genet.j;
		
		actualGenets--;
		
        setChanged();
        notifyObservers(text);
        
        if (actualGenets==0)
        {
    		running = false;
        	GameOverDialog gameover = new GameOverDialog(initialMetrics,actualMetrics);
    		gameover.setVisible(true);
        }
		
		
		
//		System.out.println("MURIO");
	}
	
	
	private final Object MOVEMENT_MUTEX= new Object();
	
	public World(int height,int width)
	{
		this.setWidth(width);
		this.setHeight(height);
		this.world = new WorldElement[height][width];
		this.terrain = new TerrainType[height][width];
		this.waitingGenets = new LinkedBlockingQueue<Genet>();
		this.listening = true;
		genetAdder.start();
		worldsListener.start();
		setRunning(true);
		this.worldsConfig = (WorldConnectionConfigLoader.loadConfig("WorldsConfig.xml"));
		setTimeMultiplier(1);
	}
	
	public TerrainType getEnvironment(WorldElement element)
	{
		int plainBucket=0;
		int waterBucket=0;
		int desertBucket=0;
		for(int i=0;i<element.getWidth();i++)
		{
			for(int j = 0; j < element.getHeigth(); j++)
			{
				switch (terrain[getTransformedI(i+element.getI())][getTransformedJ(j+element.getJ())]){
					case PLAIN:
						plainBucket++;
					break;
					case DESERT:
						desertBucket++;
					break;
					case WATER:
						waterBucket++;
						break;
					default:
						break;
				}
			}
		}
		
		
		int max = plainBucket;
		
		if(desertBucket > max)
		{
			max = desertBucket;
		}
		if(waterBucket > max)
		{
			max = waterBucket;
		}
		
		if(plainBucket == max)
		{
			return TerrainType.PLAIN;
		}
		else if(desertBucket == max)
		{
			return TerrainType.DESERT;
		}
		else 
		{
			return TerrainType.WATER;
		}
	}
	
	private boolean findSexMateUp(Genet genet)
	{
		for(int i = 0; i < genet.getWidth();i++)
		{
			int upJ = getTransformedJ(genet.getJ()-1);		
			WorldElement elementConsidered = world[getTransformedI(i+genet.getI())][upJ];
			if(elementConsidered!=null && elementConsidered instanceof Genet)
			{
				Genet neighbour = (Genet) elementConsidered;
				if(neighbour.getSex() == Sex.FEMALE)
				{
					neighbour.reproduce(genet);
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean findSexMateDown(Genet genet)
	{
		for(int i = 0; i < genet.getWidth();i++)
		{
			int downJ = getTransformedJ(genet.getJ()+genet.getHeigth());
			WorldElement elementConsidered = world[getTransformedI(i+genet.getI())][downJ];
			if(elementConsidered!=null && elementConsidered instanceof Genet)
			{
				Genet neighbour = (Genet) elementConsidered;
				if(neighbour.getSex() == Sex.FEMALE)
				{
					neighbour.reproduce(genet);
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean findSexMateLeft(Genet genet)
	{
		for(int j=0;j<genet.getHeigth();j++)
		{
			int leftI = getTransformedI(genet.getI()-1);
			WorldElement elementConsidered = world[leftI][getTransformedJ(j+genet.getJ())];
			if(elementConsidered!=null && elementConsidered instanceof Genet)
			{
				Genet neighbour = (Genet) elementConsidered;
				if(neighbour.getSex() == Sex.FEMALE)
				{
					neighbour.reproduce(genet); 
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean findSexMateRight(Genet genet)
	{
		for(int j=0;j<genet.getHeigth();j++)
		{
			int rightI = getTransformedI(genet.getI()+genet.getWidth());
			WorldElement elementConsidered = world[rightI][getTransformedJ(j+genet.getJ())];
			if(elementConsidered!=null && elementConsidered instanceof Genet)
			{
				Genet neighbour = (Genet) elementConsidered;
				if(neighbour.getSex() == Sex.FEMALE)
				{
					neighbour.reproduce(genet); 
					return true;
				}
			}
		}
		return false;
	}
	
	public void findSexMate(Genet genet)
	{
		findSexMateUp(genet);
		findSexMateRight(genet);
		findSexMateDown(genet);
		findSexMateLeft(genet);
	}
	public void setInitialPositionForGenet(Genet genet)
	{
		Random r = new Random();
		int initialZoneHorizontal = getFabrica().getI() - 1;
		int initialZoneVertical = getFabrica().getJ() - 1;

		boolean acceptableValue = false;
		int settedI = 0;
		int settedJ = 0;
		while(!acceptableValue)
		{
			settedI = r.nextInt(getFabrica().getWidth() + 2) + initialZoneHorizontal;
			settedJ = r.nextInt(getFabrica().getHeigth() + 2) + initialZoneVertical;
			if(!(settedI > initialZoneHorizontal && settedI < initialZoneHorizontal + getFabrica().getWidth() + 1 && settedI > initialZoneHorizontal && settedJ < initialZoneVertical + getFabrica().getHeigth() + 1))
			{
				acceptableValue = !acceptableValue;
				if(settedI < initialZoneHorizontal + getFabrica().getWidth() + 1 && settedJ < initialZoneVertical + getFabrica().getHeigth() + 1)
				{
					if(settedI == initialZoneHorizontal)
					{
						settedI = getTransformedI(settedI - genet.getWidth() + 1);
						settedJ = getTransformedJ(settedJ);
					}
					else
					{
						settedI = getTransformedI(settedI);
						settedJ = getTransformedJ(settedJ - genet.getHeigth() + 1);
					}
				}
				else
				{
					settedI = getTransformedI(settedI);
					settedJ = getTransformedJ(settedJ);
				}
			}
			
		}
		
		genet.setI(settedI);
		genet.setJ(settedJ);
	}
	public void enqueueNewGenet(Genet genet)
	{
		synchronized (waitingGenets) {
			waitingGenets.add(genet);
			waitingGenets.notify();	
		}
		
		actualMetrics.update(genet);
	}
	
	public void enqueueInitialNewGenet(ArrayList<Genet> genets)
	{
		for(int i=0;i<genets.size();i++)
		{
			synchronized (waitingGenets) {
				waitingGenets.add(genets.get(i));
				waitingGenets.notify();	
			}
		}
	}
	
	
	public boolean tryMove(Genet genet, int verticalDisplacement, int horizontalDisplacement)
	{
		synchronized (MOVEMENT_MUTEX) {
			int destinationI = getTransformedI(genet.getI() + horizontalDisplacement);
			int destinationJ = getTransformedJ(genet.getJ() + verticalDisplacement);
			
			//Check for free space in the destination
			for(int i = 0; i < genet.getWidth(); i++)
			{
				int testI = getTransformedI(destinationI + i);
				
				for(int j = 0; j < genet.getHeigth(); j++)
				{
					int testJ = getTransformedJ(destinationJ + j);
					if(world.length <= testI || world[testI].length <= testJ)
					{
						return false;
					}
					else if(world[testI][testJ]==null || world[testI][testJ]==genet)
					{
						continue;
					}
					else
					{
						return false;
					}
				}
			}
			
			// Coordinates to update. These coordinates will be passed to observer as arg1.
			ArrayList<Pair<Integer, Integer>> coordinates = new ArrayList<Pair<Integer, Integer>>();
			
			coordinates = moveGenet(genet, destinationI, destinationJ, coordinates);

			//Place Genet in the new place and delete it from the old one
			for(int i = 0; i < genet.getWidth(); i++)
			{
				
				int finalI = getTransformedI(destinationI + i);
				
				for(int j = 0; j < genet.getHeigth(); j++)
				{
					int finalJ = getTransformedJ(destinationJ + j);
					coordinates.add(Pair.with(finalI, finalJ));
					world[finalI][finalJ] = genet;
				}
			}
			
			
			genet.setI(destinationI);
			genet.setJ(destinationJ);
			
			// Now notify GameView to update the changes in genets.
			setChanged();
			notifyObservers(coordinates);
		}
		return true;
	}
	
	
	private ArrayList<Pair<Integer, Integer>> moveGenet(Genet genet, int destinationI, int destinationJ, ArrayList<Pair<Integer, Integer>> coordinates) {
		for(int i = 0; i < genet.getWidth(); i++)
		{
			int originalI = getTransformedI(genet.getI() + i);
			
			for(int j = 0; j < genet.getHeigth(); j++)
			{
				int originalJ = getTransformedJ(genet.getJ() + j);					
				coordinates.add(Pair.with(originalI, originalJ));
				world[originalI][originalJ] = null;
			}
		}
		return coordinates;
	}

	/**
	 * @param i row
	 * @return the row between the world dimensions
	 */
	public int getTransformedI(int i)
	{
		int transformed = i%getWidth();
		if (transformed < 0)
		{
			return transformed+getWidth();
		}
		else
		{
			return transformed;
		}
	}
	
	/**
	 * @param j column
	 * @return the column between the world dimensions
	 */
	public int getTransformedJ(int j)
	{
		int transformed = j%getHeight();
		if (transformed < 0)
		{
			return transformed + getHeight();
		}
		else
		{
			return transformed;
		}
	}
	
	/**
	 * Sets and fills the terrain types of the World
	 * @param hasDesert if the user selected the existence of DESERT terrain
	 * @param hasWater if the user selected the existence of WATER terrain
	 * @param hasPlain if the user selected the existence of PLAIN terrain
	 */
	public void setInitialTerrain(boolean hasDesert, boolean hasWater, boolean hasPlain)
	{
		Random r = new Random();
		ArrayList<TerrainType> terrains = new ArrayList<TerrainType>();
		
		if(!hasPlain && !hasWater && !hasDesert)
		{
			terrains.add(TerrainType.PLAIN);
		}
		else{
			if(hasPlain)
			{
				terrains.add(TerrainType.PLAIN);
			}
			if(hasWater)
			{
				terrains.add(TerrainType.WATER);
			}
			if(hasDesert)
			{
				terrains.add(TerrainType.DESERT);
			}
		}
		
		fillRectangle(0, 0, getWidth(), getHeight(), terrains.get(0));
		
		int horizontalMaximum = getWidth() / 2;
		int verticalMaximum = getHeight() / 2;
		
		int firstSubDivision = r.nextInt(3);
		int secondSubDivision = firstSubDivision;
		while (secondSubDivision == firstSubDivision)
		{
			secondSubDivision = r.nextInt(3);
		}
		if(terrains.size() > 1){
			subDivision(1, firstSubDivision, horizontalMaximum, verticalMaximum, terrains);
		}
		if(terrains.size() > 2){
			subDivision(2, secondSubDivision, horizontalMaximum, verticalMaximum, terrains);
		}
	}

	public void subDivision(int terrainNumber, int subDivision, int horizontalMaximum, int verticalMaximum, ArrayList<TerrainType> terrains){
		Random r = new Random();
		int subTerrainWidth, subTerrainHeight, subTerrainWidthZero, subTerrainHeightZero;
		subTerrainWidth = r.nextInt(horizontalMaximum - horizontalMaximum/2) + horizontalMaximum/2;
		subTerrainHeight = r.nextInt(verticalMaximum - verticalMaximum/2) + verticalMaximum/2;
		String test = ""+subDivision+""+terrainNumber+"";
		if(test.equals("01")|| test.equals("02"))
		{
			subTerrainWidthZero = r.nextInt(horizontalMaximum - subTerrainWidth);
			subTerrainHeightZero = r.nextInt(verticalMaximum - subTerrainHeight);
		}
		else if(test.equals("11") || test.equals("12"))
		{
			subTerrainWidthZero = r.nextInt(getWidth() - (subTerrainWidth + horizontalMaximum)) + horizontalMaximum;
			subTerrainHeightZero = r.nextInt(verticalMaximum - subTerrainHeight);
		}
		else if(test.equals("21")|| test.equals("22"))
		{
			subTerrainWidthZero = r.nextInt(horizontalMaximum - subTerrainWidth);
			subTerrainHeightZero = r.nextInt(getHeight() - (subTerrainHeight + verticalMaximum)) + verticalMaximum;
		}
		else
		{
			subTerrainWidthZero = r.nextInt(getWidth() - (subTerrainWidth + horizontalMaximum)) + horizontalMaximum;
			subTerrainHeightZero = r.nextInt(getHeight() - (subTerrainHeight + verticalMaximum)) + verticalMaximum;
		}
		fillRectangle(subTerrainWidthZero, subTerrainHeightZero, subTerrainWidth, subTerrainHeight, terrains.get(terrainNumber));
	
	}

	/**
	 * Method used as an algorithm to fill a rectangular section with a single terrainType
	 * @param horizontalZero
	 * @param verticalZero
	 * @param terrainWidth
	 * @param terrainHeight
	 * @param terrainTypeToFill
	 */
	public void fillRectangle(int horizontalZero, int verticalZero, int terrainWidth, int terrainHeight, TerrainType terrainTypeToFill)
	{
		for(int i = verticalZero; i < terrainHeight + verticalZero; i++)
		{
			for(int j = horizontalZero; j < terrainWidth + horizontalZero; j++)
			{
				terrain[i][j] = terrainTypeToFill;
			}
		}
	}
	
	public WorldElement[][] getWorld()
	{
		return world;
	}
	public void setWorld(WorldElement[][] newWorld)
	{
		world = newWorld;
	}

	public TerrainType[][] getTerrain()
	{
		return terrain;
	}
	public void setTerrain(TerrainType[][] t)
	{
		terrain = t;
	}
	
	public TerrainType getField(int x,int y)
	{
		return terrain[x][y];
	}
	public void setField(int x, int y, TerrainType field)
	{
		terrain[x][y] = field;
	}
	
	/**
	 * Returns the number of coins that the user has collected.
	 * @return number of coins
	 */
	public int getCoins()
	{
		return coins;
	}
	
	/**
	 * Sets the number of coins that the user has collected.
	 */
	public void setCoins(int coins)
	{
		this.coins = coins;
	}
	
//	public Genet createAGenet()
//	{
//
//	}

	public Genet createGenetdeLlanura()
	{
			Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();
			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();

			Genotype genotype;


				skinColorAlleles = "150,150";
				speedAlleles = "" + (3) + "," + (3) + "";
				terrainAffinityAlleles = "P,P";
				lifeSpanAlleles = "" + (25 + 50) + "," + (25 + 1) + "";
				
				if(initialGens.contains("Vida"))
				{
					genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				}
				if(initialGens.contains("Movimiento"))
				{
					genotypeBuilder.speedGen(speedAlleles);
				}
				if(initialGens.contains("Afinidad con terreno"))
				{
					genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				}
				if(initialGens.contains("Color"))
				{
					genotypeBuilder.skinColorGen(skinColorAlleles);
				}
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				genotype = genotypeBuilder.build();
				
				genet = genetBuilder.
						genotype(genotype).
						height(2).
						width(2).
						liveBehavior(new DefaultLiveBehavior()).
						moveBehavior(new DefaultMoveBehavior()).
						phenotype(new BiologicalAccuratePhenotype()).
						reproductionBehavior(new DefaultReproductionBehavior()).
						world(this).
						build();
				return genet;
	}

		public Genet createGenetdeAgua()
	{
			Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();
			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();

			Genotype genotype;


				skinColorAlleles = "255,255";
				speedAlleles = "" + (1) + "," + (1) + "";
				terrainAffinityAlleles = "W,W";
				lifeSpanAlleles = "" + (25 + 50) + "," + (25 + 1) + "";
				
				if(initialGens.contains("Vida"))
				{
					genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				}
				if(initialGens.contains("Movimiento"))
				{
					genotypeBuilder.speedGen(speedAlleles);
				}
				if(initialGens.contains("Afinidad con terreno"))
				{
					genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				}
				if(initialGens.contains("Color"))
				{
					genotypeBuilder.skinColorGen(skinColorAlleles);
				}
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				genotype = genotypeBuilder.build();
				
				genet = genetBuilder.
						genotype(genotype).
						height(2).
						width(2).
						liveBehavior(new DefaultLiveBehavior()).
						moveBehavior(new DefaultMoveBehavior()).
						phenotype(new BiologicalAccuratePhenotype()).
						reproductionBehavior(new DefaultReproductionBehavior()).
						world(this).
						build();
				return genet;
	}

			public Genet createGenetdeDesierto()
	{
			Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();
			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();

			Genotype genotype;


						skinColorAlleles = "0,0";
				speedAlleles = "" + (2) + "," + (2) + "";
				terrainAffinityAlleles = "D,D";
				lifeSpanAlleles = "" + (25 + 50) + "," + (25 + 1) + "";
				
				if(initialGens.contains("Vida"))
				{
					genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				}
				if(initialGens.contains("Movimiento"))
				{
					genotypeBuilder.speedGen(speedAlleles);
				}
				if(initialGens.contains("Afinidad con terreno"))
				{
					genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				}
				if(initialGens.contains("Color"))
				{
					genotypeBuilder.skinColorGen(skinColorAlleles);
				}
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				genotype = genotypeBuilder.build();
				
				genet = genetBuilder.
						genotype(genotype).
						height(2).
						width(2).
						liveBehavior(new DefaultLiveBehavior()).
						moveBehavior(new DefaultMoveBehavior()).
						phenotype(new BiologicalAccuratePhenotype()).
						reproductionBehavior(new DefaultReproductionBehavior()).
						world(this).
						build();
				return genet;
	}

	public Genet createGenetDuraderoyFuerte()
	{

		Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();
			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();

			Genotype genotype;
						skinColorAlleles = "0,0";
				speedAlleles = "" + (4) + "," + (4) + "";
				terrainAffinityAlleles = "";
				lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
				terrainAffinityNumber = 0;
				for (int i = 0; i < 2; i++)
				{
					terrainAffinityNumber = r.nextInt(3);
					if(terrainAffinityNumber == 0)
					{
						terrainAffinityAlleles += "W";
					}
					else if(terrainAffinityNumber == 1)
					{
						terrainAffinityAlleles += "D";
					}
					else
					{
						terrainAffinityAlleles += "P";
					}
					
					if(i==0)
					{
						terrainAffinityAlleles += ",";
					}
				}
				
				if(initialGens.contains("Vida"))
				{
					genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				}
				if(initialGens.contains("Movimiento"))
				{
					genotypeBuilder.speedGen(speedAlleles);
				}
				if(initialGens.contains("Afinidad con terreno"))
				{
					genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				}
				if(initialGens.contains("Color"))
				{
					genotypeBuilder.skinColorGen(skinColorAlleles);
				}
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				genotype = genotypeBuilder.build();
				
				genet = genetBuilder.
						genotype(genotype).
						height(2).
						width(2).
						liveBehavior(new DefaultLiveBehavior()).
						moveBehavior(new DefaultMoveBehavior()).
						phenotype(new BiologicalAccuratePhenotype()).
						reproductionBehavior(new DefaultReproductionBehavior()).
						world(this).
						build();
						return genet;

	}

		public Genet createGenetMonstruo()
	{

		Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();
			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();

			Genotype genotype;
			skinColorAlleles = "0,255";
				speedAlleles = "" + (2) + "," + (2) + "";
				terrainAffinityAlleles = "P,D";
				lifeSpanAlleles = "" + (25 + 50) + "," + (25 + 1) + "";
				
				if(initialGens.contains("Vida"))
				{
					genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				}
				if(initialGens.contains("Movimiento"))
				{
					genotypeBuilder.speedGen(speedAlleles);
				}
				if(initialGens.contains("Afinidad con terreno"))
				{
					genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				}
				if(initialGens.contains("Color"))
				{
					genotypeBuilder.skinColorGen(skinColorAlleles);
				}
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				genotype = genotypeBuilder.build();
				
				genet = genetBuilder.
						genotype(genotype).
						height(4).
						width(4).
						liveBehavior(new DefaultLiveBehavior()).
						moveBehavior(new DefaultMoveBehavior()).
						phenotype(new BiologicalAccuratePhenotype()).
						reproductionBehavior(new DefaultReproductionBehavior()).
						world(this).
						build();
						return genet;

	}

		public Genet createGenetMutante()
	{

		Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();
			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();

			Genotype genotype;
			skinColorAlleles = "0,255";
				speedAlleles = "" + (2) + "," + (2) + "";
				terrainAffinityAlleles = "P,D";
				lifeSpanAlleles = "" + (25 + 50) + "," + (25 + 1) + "";
				
				if(initialGens.contains("Vida"))
				{
					genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				}
				if(initialGens.contains("Movimiento"))
				{
					genotypeBuilder.speedGen(speedAlleles);
				}
				if(initialGens.contains("Afinidad con terreno"))
				{
					genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				}
				if(initialGens.contains("Color"))
				{
					genotypeBuilder.skinColorGen(skinColorAlleles);
				}
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				genotype = genotypeBuilder.build();
				
				genet = genetBuilder.
						genotype(genotype).
						height(2).
						width(1).
						liveBehavior(new DefaultLiveBehavior()).
						moveBehavior(new DefaultMoveBehavior()).
						phenotype(new BiologicalAccuratePhenotype()).
						reproductionBehavior(new DefaultReproductionBehavior()).
						world(this).
						build();
						return genet;

	}

			public Genet createGenetLongevo()
	{

		Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();
			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();

			Genotype genotype;
	skinColorAlleles = "150,150";
				speedAlleles = "" + (1) + "," + (1) + "";
				terrainAffinityAlleles = "P,D";
				lifeSpanAlleles = "" + (99) + "," + (99) + "";
				
				if(initialGens.contains("Vida"))
				{
					genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				}
				if(initialGens.contains("Movimiento"))
				{
					genotypeBuilder.speedGen(speedAlleles);
				}
				if(initialGens.contains("Afinidad con terreno"))
				{
					genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				}
				if(initialGens.contains("Color"))
				{
					genotypeBuilder.skinColorGen(skinColorAlleles);
				}
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				genotype = genotypeBuilder.build();
				
				genet = genetBuilder.
						genotype(genotype).
						height(2).
						width(2).
						liveBehavior(new DefaultLiveBehavior()).
						moveBehavior(new DefaultMoveBehavior()).
						phenotype(new BiologicalAccuratePhenotype()).
						reproductionBehavior(new DefaultReproductionBehavior()).
						world(this).
						build();
						return genet;

	}


			public Genet createGenetFugaz()
	{

		Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();
			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();

			Genotype genotype;
	skinColorAlleles = "200,200";
				speedAlleles = "" + (4) + "," + (4) + "";
				terrainAffinityAlleles = "P,D";
				lifeSpanAlleles = "" + (1) + "," + (1) + "";
				
				if(initialGens.contains("Vida"))
				{
					genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				}
				if(initialGens.contains("Movimiento"))
				{
					genotypeBuilder.speedGen(speedAlleles);
				}
				if(initialGens.contains("Afinidad con terreno"))
				{
					genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				}
				if(initialGens.contains("Color"))
				{
					genotypeBuilder.skinColorGen(skinColorAlleles);
				}
				
				if(r.nextBoolean())
				{
					sexAlleles = "X,X";
					genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				}
				else
				{
					sexAlleles = "X,Y";
					genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				}
				genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				genotype = genotypeBuilder.build();
				
				genet = genetBuilder.
						genotype(genotype).
						height(2).
						width(2).
						liveBehavior(new DefaultLiveBehavior()).
						moveBehavior(new DefaultMoveBehavior()).
						phenotype(new BiologicalAccuratePhenotype()).
						reproductionBehavior(new DefaultReproductionBehavior()).
						world(this).
						build();
						return genet;

	}

	/**
	 * Adds a genet paid with coins.
	 * @param genet
	 * @return 
	 */
	public boolean createGenetByCoins(int indexGenet)
	{
		if(coins > 100)
		{
			Genet genet = null;
			
			String sexAlleles;
			String skinColorAlleles = "0,0";
			String speedAlleles = "" + (4) + "," + (4) + "";
			String terrainAffinityAlleles = "";
			String lifeSpanAlleles = "" + (48 + 50) + "," + (48 + 1) + "";
			int terrainAffinityNumber = 0;
			Random r = new Random();
			
			GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
			GenetBuilder genetBuilder = new GenetBuilder();

			
			ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();
			
			Genotype genotype;
			
			switch(indexGenet)
			{
			case 0:
				createGenetByCoins();
				break;

				// Genet de llanura
//				case 1:
				// skinColorAlleles = "150,150";
				// speedAlleles = "" + (3) + "," + (3) + "";
				// terrainAffinityAlleles = "P,P";
				// lifeSpanAlleles = "" + (25 + 50) + "," + (25 + 1) + "";
				
				// if(initialGens.contains("Vida"))
				// {
				// 	genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				// }
				// if(initialGens.contains("Movimiento"))
				// {
				// 	genotypeBuilder.speedGen(speedAlleles);
				// }
				// if(initialGens.contains("Afinidad con terreno"))
				// {
				// 	genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				// }
				// if(initialGens.contains("Color"))
				// {
				// 	genotypeBuilder.skinColorGen(skinColorAlleles);
				// }
				
				// if(r.nextBoolean())
				// {
				// 	sexAlleles = "X,X";
				// 	genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				// }
				// else
				// {
				// 	sexAlleles = "X,Y";
				// 	genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				// }
				// genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				// genotype = genotypeBuilder.build();
				
				// genet = genetBuilder.
				// 		genotype(genotype).
				// 		height(2).
				// 		width(2).
				// 		liveBehavior(new DefaultLiveBehavior()).
				// 		moveBehavior(new DefaultMoveBehavior()).
				// 		phenotype(new BiologicalAccuratePhenotype()).
				// 		reproductionBehavior(new DefaultReproductionBehavior()).
				// 		world(this).
				// 		build();
				// genet = createGenetdeLlanura();
				// break;	

				// Genet de agua
				case 1:
				genet = createGenetdeAgua();
				break;

				// Genet de desierto
				case 2:
				genet = createGenetdeDesierto();
				break;	

				// duradero y fuerte...
				case 3:
				genet = createGenetDuraderoyFuerte();
				break;

				// Genet monstruo
			case 4:
			genet = createGenetMonstruo();
				break;	



				// Genet fantasma
				// case 3:
				// skinColorAlleles = "255,255";
				// speedAlleles = "" + (2) + "," + (2) + "";
				// terrainAffinityAlleles = "P,D";
				// lifeSpanAlleles = "" + (25 + 50) + "," + (25 + 1) + "";
				
				// if(initialGens.contains("Vida"))
				// {
				// 	genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
				// }
				// if(initialGens.contains("Movimiento"))
				// {
				// 	genotypeBuilder.speedGen(speedAlleles);
				// }
				// if(initialGens.contains("Afinidad con terreno"))
				// {
				// 	genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
				// }
				// if(initialGens.contains("Color"))
				// {
				// 	genotypeBuilder.skinColorGen(skinColorAlleles);
				// }
				
				// if(r.nextBoolean())
				// {
				// 	sexAlleles = "X,X";
				// 	genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
				// }
				// else
				// {
				// 	sexAlleles = "X,Y";
				// 	genetBuilder.runBehavior(new DefaultMaleRunBehavior());
				// }
				// genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
				
				// genotype = genotypeBuilder.build();
				
				// genet = genetBuilder.
				// 		genotype(genotype).
				// 		height(2).
				// 		width(2).
				// 		liveBehavior(new DefaultLiveBehavior()).
				// 		moveBehavior(new DefaultMoveBehavior()).
				// 		phenotype(new BiologicalAccuratePhenotype()).
				// 		reproductionBehavior(new DefaultReproductionBehavior()).
				// 		world(this).
				// 		build();
				// break;	
				
				// Genet mutante
				case 5:
				genet = createGenetMutante();
				break;	

				// Genet transitorio(es decir solamente tiene lifespan de 1).
				
				case 6:
				genet = createGenetLongevo();
				break; 
				
				case 7:
				genet = createGenetFugaz();
				break;	
			}
			
			
			enqueueNewGenet(genet);
			coins -= 100;
			return true;
		}
		else
			return false;


	}
	
	/**
	 * Asks if the player has enough coins (100) to create a new Genet.
	 * If so, it creates a new Genet randomly, and takes aways 100 coins
	 * from the player and returns true. If not, it returns false.
	 * @return a boolean that determines if the new Genet was created or not.
	 */
	public boolean createGenetByCoins()
	{
		if(coins > 100)
		{
			Random r = new Random();
			Genet genet = createRandomGenet(r);
			enqueueNewGenet(genet);
			coins -= 100;
			return true;
		}
		else
		{
			return false;
		}
	}
	/**
	 * Creates a new Genet without having genetic information from parents. This should be used
	 * when creating the first Genets of the game, or when the user wants to create a new Genet
	 * with his coins.
	 * @param r
	 */
	public Genet createRandomGenet(Random r)
	{
		String sexAlleles;
		String skinColorAlleles = "" + r.nextInt(255) + "," + r.nextInt(255) + "";
		String speedAlleles = "" + (r.nextInt(4) + 1) + "," + (r.nextInt(4) + 1) + "";
		String terrainAffinityAlleles = "";
		String lifeSpanAlleles = "" + (r.nextInt(49) + 50) + "," + (r.nextInt(49) + 1) + "";
		int terrainAffinityNumber = 0;
		for (int i = 0; i < 2; i++)
		{
			terrainAffinityNumber = r.nextInt(3);
			if(terrainAffinityNumber == 0)
			{
				terrainAffinityAlleles += "W";
			}
			else if(terrainAffinityNumber == 1)
			{
				terrainAffinityAlleles += "D";
			}
			else
			{
				terrainAffinityAlleles += "P";
			}
			
			if(i==0)
			{
				terrainAffinityAlleles += ",";
			}
		}
		
		GenotypeBuilder genotypeBuilder = new GenotypeBuilder();
		GenetBuilder genetBuilder = new GenetBuilder();
		
		ArrayList<String> initialGens = GameView.getGameConfig().getActiveGens();
		if(initialGens.contains("Vida"))
		{
			genotypeBuilder.lifeSpanGen(lifeSpanAlleles);
		}
		if(initialGens.contains("Movimiento"))
		{
			genotypeBuilder.speedGen(speedAlleles);
		}
		if(initialGens.contains("Afinidad con terreno"))
		{
			genotypeBuilder.terrainAffinityGen(terrainAffinityAlleles);
		}
		if(initialGens.contains("Color"))
		{
			genotypeBuilder.skinColorGen(skinColorAlleles);
		}
		
		if(r.nextBoolean())
		{
			sexAlleles = "X,X";
			genetBuilder.runBehavior(new DefaultFemaleRunBehavior());
		}
		else
		{
			sexAlleles = "X,Y";
			genetBuilder.runBehavior(new DefaultMaleRunBehavior());
		}
		genotypeBuilder = genotypeBuilder.sexGen(sexAlleles);
		
		Genotype genotype = genotypeBuilder.build();
		
		Genet genet;
		
		genet = genetBuilder.
				genotype(genotype).
				height(2).
				width(2).
				liveBehavior(new DefaultLiveBehavior()).
				moveBehavior(new DefaultMoveBehavior()).
				phenotype(new BiologicalAccuratePhenotype()).
				reproductionBehavior(new DefaultReproductionBehavior()).
				world(this).
				build();

		return genet;
	}
	
	
	public void addInitialGenets(int initialGenets, Random r)
	{
		ArrayList<Genet> genets = new ArrayList<>();
		for(int i=0; i < initialGenets;i++)
		{
			Genet genet = createRandomGenet(r);
			genets.add(genet);
		}
		
		initialMetrics = new WorldMetrics(genets);
		actualMetrics = initialMetrics.clone();

		enqueueInitialNewGenet(genets);
	}
//	public void update(Observable obs, Object arg1)
//	{
//		if (obs instanceof Genet)
//		{
//			Genet genet = (Genet) obs;
//			
//		}
//	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	
	public Genet getGenetAt(int i, int j)
	{
		WorldElement element = world[i][j];
		if(element instanceof Genet)
			return (Genet)element;
		
		return null;
	}
	
	public TerrainType getTerrainAt(int i, int j)
	{
			return terrain[i][j];
	}

	public void setTerrainAt(int i, int j, TerrainType newterrain)
	{
			terrain[i][j] = newterrain;
	}

	public List<WorldConnectionConfig> getWorldsConfig() {
		return worldsConfig;
	}

	public Obstacle getFabrica() {
		return fabrica;
	}

	public void setFabrica(Obstacle fabrica) {
		this.fabrica = fabrica;
		this.fabrica.setFabrica();
		ArrayList<Pair<Integer, Integer>> coordinates = new ArrayList<Pair<Integer, Integer>>();
		for(int hor = fabrica.getI(); hor < fabrica.getI()+fabrica.getWidth(); hor++){
			for(int ver = fabrica.getJ(); ver < fabrica.getJ() + fabrica.getHeigth(); ver++){
				coordinates.add(Pair.with(hor,ver));
				world[getTransformedI(hor)][getTransformedJ(ver)] = fabrica;
			}
		}
		setChanged();
		notifyObservers(coordinates);
	}
	public void enqueueInitialNewGenet(Genet genet)
	{
		synchronized (waitingGenets) {
			waitingGenets.add(genet);
			waitingGenets.notify();	
		}
	}
	
	public void setObstacle(Obstacle obstacle) {
		//obstacles.add(obstacle);
		ArrayList<Pair<Integer, Integer>> coordinates = new ArrayList<Pair<Integer, Integer>>();
		for(int hor = obstacle.getI(); hor < obstacle.getI()+obstacle.getWidth(); hor++){
			for(int ver = obstacle.getJ(); ver < obstacle.getJ() + obstacle.getHeigth(); ver++){
				coordinates.add(Pair.with(hor,ver));
				world[getTransformedI(hor)][getTransformedJ(ver)] = obstacle;
			}
		}
		setChanged();
		notifyObservers(coordinates);
	}
	
	public WorldMetrics getInitialMetrics()
	{
		return initialMetrics;
	}
	
	public WorldMetrics getActualMetrics()
	{
		return actualMetrics;
	}
	
	public int getActualGenetsCount()
	{
		return actualGenets;
	}

	public double getTimeMultiplier() {
		return timeMultiplier;
	}

	public void setTimeMultiplier(double timeMultiplier) {
		this.timeMultiplier = timeMultiplier;
	}
	
}
