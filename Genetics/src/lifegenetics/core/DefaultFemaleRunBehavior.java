package lifegenetics.core;

/**
 * Default run behavior of female Genets
 */
public class DefaultFemaleRunBehavior implements RunBehavior{

	/** (non-Javadoc)
	 * @see RunBehavior#run(Genet)
	 */
	@Override
	public void run(Genet genet) {
		// TODO Auto-generated method stub
		
 		if(genet.isInReproductiveCooldown())
 		{
 			genet.decrementReproductiveCooldown();
 		}
 		genet.getMoveBehavior().move(genet);
 		genet.getLiveBehavior().adjustHealth(genet);
	}

	@Override public String toString()
	{
		return "DefaultFemaleRunBehavior";
	}

}
