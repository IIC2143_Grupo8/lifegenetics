package lifegenetics.core;

import java.util.ArrayList;

public class GameConfiguration {

	private int worldSize;
	private int initialGenets;
	private int initialObstacles;
	private ArrayList<TerrainType> terrainTypes;
	private ArrayList<String> activeGens;

	public static final String[] availableGenList ={"Vida" , "Movimiento", "Afinidad con terreno", "Color"};
	
	/**
	 * Stores the game configuration parameters.
	 * @param worldSize world's height and width (the world is square)
	 * @param initialGenets 
	 * @param terrainTypes list of the terrain types that are included in the simulation
	 * @param activeGens list of the gens that will be active during the simulation. 
	 * If a gen is not selected, then all the genets will have the same values for the expresion of that gen
	 */
	public GameConfiguration(int worldSize, int initialGenets, int initialObstacles, ArrayList<TerrainType> terrainTypes, ArrayList<String> activeGens)
	{
		this.worldSize = worldSize;
		this.initialGenets = initialGenets;
		this.initialObstacles = initialObstacles;
		this.terrainTypes = terrainTypes;
		this.activeGens = activeGens;
	}

	
	public int getWorldSize() {
		return worldSize;
	}
	
	public void setWorldSize(int newTamanoMundo)
	{
		worldSize = newTamanoMundo;
	}


	public int getInitialGenets() {
		return initialGenets;
	}
	
	public int getInitialObstacles(){
		return initialObstacles;
	}


	public ArrayList<TerrainType> getTerrainTypes() {
		return terrainTypes;
	}
	
	
	public ArrayList<String> getActiveGens(){
		return activeGens;
	}

}
