package lifegenetics.core;

/**
 * Defines the methods that will be executed in a cycle
 */
public interface RunBehavior {
	/**
	 * @param genet to runs
	 */
	void run(Genet genet);

}
