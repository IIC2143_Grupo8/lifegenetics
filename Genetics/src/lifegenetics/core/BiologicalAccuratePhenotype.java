package lifegenetics.core;

public class BiologicalAccuratePhenotype extends Phenotype {
	
	@Override
	
	/**
	 * To obtain the actual speed of the Genet, this method analyzes both 
	 * alleles. The speed is maximum when it is 1, and when it raises its
	 * value, it means that it takes more time to move.
	 * 
	 * If the difference between the both alleles is over 2 speed units, it's 
	 * considered as a dominance from the greater allele over the other.
	 * If not, the method calculates the average of both numbers.
	 * 
	 * To consider the environment, if the environment where the Genet
	 * is standing is the same as the kind of terrain with which it
	 * has an affinity, then it lowes its value on 1 (only if it's at least 2)
	 * 
	 * If not, and if it's standing on his opposite terrain, it adds
	 * 1 speed unit.
	 * 
	 * Possible Alleles: Integers from 1 or more
	 * 
	 * @param genotype of the calling Genet
	 * @param environment of the calling Genet
	 * @return the speed in cell numbers of the calling Genet
	 */
	
	int getSpeed(Genotype genotype, TerrainType environment) {
		// TODO Auto-generated method stub
		String speedGen = genotype.getSpeedGen().getGen();
		String[] alleleInText;
		alleleInText = speedGen.split(",");
		int[] allele = new int[alleleInText.length];
		for(int alleleCounter = 0; alleleCounter < 2; alleleCounter++)
		{
			allele[alleleCounter] = Integer.parseInt(alleleInText[alleleCounter]);
		}
		int speed = 0;
		TerrainType terrainAffinity = getTerrainAffinity(genotype);
		if(Math.abs(allele[0] - allele[1]) > 2)
		{
			speed = Math.max(allele[0], allele[1]);
		}
		else
		{
			speed = Math.floorDiv(allele[0] + allele[1] , 2);
		}
		if(terrainAffinity == environment && speed > 1)
		{
			speed -= 1;
		}
		else if(environment != TerrainType.PLAIN)
		{
			speed += 1;
		}	
		return speed;
	}

	@Override
	
	/**
	 * To obtain the terrain with which the Genet has affinity, this method 
	 * analyzes both alleles. 
	 * 
	 * Alleles with terrains DESERT or WATER are dominant, and alleles with
	 * a PLAIN terrain are recessive
	 * 
	 * If there is codominance (both alleles are dominant, and of different
	 * kind) or double recessive, then the type of terrain will be PLAIN.
	 * If not, the dominant allele will give the correct phenotype.
	 * 
	 * Possible Alleles: "P" for PLAIN, "W" for WATER, "D" for DESERT as Strings
	 * 
	 * @param genotype of the calling Genet
	 * @return the type of terrain with which the calling Genet has affinity
	 */
	
	TerrainType getTerrainAffinity(Genotype genotype) {
		// TODO Auto-generated method stub
		String terrainAffinityGen = genotype.getTerrainAffinityGen().getGen();
		String[] allele;
		allele = terrainAffinityGen.split(",");
		TerrainType terrain;
		if((allele[0].equals("D") && allele[0].equals("W")) || (allele[0].equals("W") && allele[0].equals("D")) || (allele[0].equals("P") && allele[0].equals("P")))
		{
			terrain = TerrainType.PLAIN;
		}
		else if(allele[0].equals("D") || allele[0].equals("D"))
		{
			terrain = TerrainType.DESERT;
		}
		else
		{
			terrain = TerrainType.WATER;
		}
		return terrain;
	}

	@Override
	
	/**
	 * To obtain the sex of a given Genet, this method analyzes both alleles.
	 * 
	 * If it is a combination of 2 'X', then its sex is set as FEMALE.
	 * If it is a combination of 'X' and 'Y', then its sex is set as MALE.
	 * 
	 * Possible Alleles: 'X' or 'Y' as String
	 * 
	 * @param genotype of the calling Genet
	 * @return the sex of the calling Genet
	 */
	
	Sex getSex(Genotype genotype) {
		// TODO Auto-generated method stub
		String sexGen = genotype.getSexGen().getGen();
		String[] allele;
		allele = sexGen.split(",");
		if (allele[0].equals("X") && allele[1].equals("X"))
		{
			return Sex.FEMALE;
		}
		else 
		{
			return Sex.MALE;
		}
	}
	
	@Override
	
	/**
	 * To obtain the lifeSpan of a given Genet, this method analyzes both
	 * alleles and its sex.
	 * 
	 * The result of the lifeSpan is set as an average of both alleles, with a
	 * slight difference depending on which sex the Genet has. If it's FEMALE,
	 * then the formula is: (allele0 + allele1 + 10) / 2. If it's MALE, then it's:
	 * (allele0 + allele1 - 10) / 2.
	 * 
	 * Possible Alleles: Positive Integers
	 * 
	 * @param genotype of the calling Genet
	 * @return the lifeSpan in thicks of the calling Genet
	 */
	
	int getLifeSpan(Genotype genotype) {
		// TODO Auto-generated method stub
		String lifeSpanGen = genotype.getLifeSpanGen().getGen();
		String[] alleleInText;
		alleleInText = lifeSpanGen.split(",");
		int[] allele = new int[alleleInText.length];
		for(int alleleCounter = 0; alleleCounter < 2; alleleCounter++)
		{
			allele[alleleCounter] = Integer.parseInt(alleleInText[alleleCounter]);
		}
		
		int lifeSpan = 0;
		Sex sex = getSex(genotype);
		if(sex == Sex.FEMALE)
		{
			lifeSpan = Math.floorDiv(allele[0] + allele[1] + 10 , 2);

		}
		else
		{
			lifeSpan = Math.floorDiv(allele[0] + allele[1] - 10 , 2);
		}
		if (lifeSpan <= 0)
			lifeSpan = 5;	
		return lifeSpan;
	}

	@Override
	
	/**
	 * To obtain the skinColor of a given Genet, this method analyzes both
	 * alleles and the kind of terrain where it is at the moment.
	 * 
	 * If there is a difference of at least 100 between both alleles, then
	 * there is a dominance of the one with the greater value (lighter color).
	 * 
	 * If not, there is codominance and the resulting color is the average
	 * between the alleles.
	 * 
	 * Also, if the actual terrain is DESERT, and the Genet doesn't have an 
	 * affinity for that kind of terrain, then its skinColor decreases in value
	 * by 50 (becoming darker).
	 * 
	 * Possible Alleles: Integers between 0 and 255
	 * 
	 * @param genotype of the calling Genet
	 * @param environment of the calling Genet
	 * @return the skin color in gray scale of the calling Genets
	 */
	
	int getSkinColor(Genotype genotype, TerrainType environment) {
		// TODO Auto-generated method stub
		String skinColorGen = genotype.getSkinColorGen().getGen();
		String[] alleleInText;
		alleleInText = skinColorGen.split(",");
		int[] allele = new int[alleleInText.length];
		for(int alleleCounter = 0; alleleCounter < 2; alleleCounter++)
		{
			allele[alleleCounter] = Integer.parseInt((alleleInText[alleleCounter]));
		}
		int color = 0;
		if(Math.abs(allele[0] - allele[1]) > 99)
		{
			color = Math.max(allele[0], allele[1]);
		}
		else
		{
			color = Math.floorDiv(allele[0] + allele[1] , 2);
		}
		if(environment.equals(TerrainType.DESERT) && !getTerrainAffinity(genotype).equals(TerrainType.DESERT))
		{
			if(color < 50)
			{
				color = 0;
			}
			else
			{
				color -= 50;
			}
		}
		return color;
	}
	
	@Override public String toString()
	{
		return "BiologicalAccuratePhenotype";
	}
	
}
