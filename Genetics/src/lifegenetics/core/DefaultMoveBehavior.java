package lifegenetics.core;

import java.util.Random;

public class DefaultMoveBehavior implements MoveBehavior{

	@Override
	public void move(Genet genet) {
		// TODO Auto-generated method stub
		/* This method generate the new position of the genet
		/* The value of the variable speed varies between 0 and 5 with 5 being the value the slower and faster 0
		 * */
		if(genet.getWaitToMove() == 0){
			// genet wait all the turns to wait and need move
			int speed = genet.getSpeed();
			String direction = generateDirection(genet.getMoveTrend());
			int verticalDisplacement = generateVerticalDisplacement(direction);
			int horizontalDisplacement = generateHorizontalDisplacement(direction);
			
			boolean genetHasMoved = genet.tryMove(verticalDisplacement, horizontalDisplacement);
			if(genetHasMoved)
			{
				if(verticalDisplacement > 0)
				{
					genet.setMoveTrend("DOWN");
				}
				else if(verticalDisplacement < 0)
				{
					genet.setMoveTrend("UP");
				}
				else if(horizontalDisplacement > 0)
				{
					genet.setMoveTrend("RIGHT");
				}
				else if(horizontalDisplacement < 0)
				{
					genet.setMoveTrend("LEFT");
				}
				else
				{
					genet.setMoveTrend("");
				}
				genet.setWaitToMove(speed); // set the initial waiting
			}
			
		} else if (genet.getWaitToMove() > 0){
			// one turn less to wait for move
			int newWait = genet.getWaitToMove()-1;
			genet.setWaitToMove(newWait); // setting the new wait
		}else{
			// if we are in any strange case (wait<0 or wait>speed) we set the speed to normalize
			genet.setWaitToMove(genet.getSpeed());
		}
	}

	private String generateDirection(String trend)
	{
		Random r = new Random();
		int generatedValue = 1 + r.nextInt(10);
		if(generatedValue < 8 && !trend.isEmpty()){
			return trend;
		}
		else
		{
			generatedValue = r.nextInt(4);
			if(generatedValue == 0)
			{
				return "UP";
			}
			else if(generatedValue == 1)
			{
				return "DOWN";
			}
			else if(generatedValue == 2)
			{
				return "LEFT";
			}
			else
			{
				return "RIGHT";
			}
		}
	}
	
	private int generateVerticalDisplacement(String direction)
	{
		if(direction.equals("UP"))
		{
			return -1;
		}
		else if(direction.equals("DOWN"))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	private int generateHorizontalDisplacement(String direction)
	{
		if(direction.equals("LEFT"))
		{
			return -1;
		}
		else if(direction.equals("RIGHT"))
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	@Override public String toString()
	{
		return "DefaultMoveBehavior";
	}

}
