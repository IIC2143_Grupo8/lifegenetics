package lifegenetics.views;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import lifegenetics.core.GameConfiguration;
import lifegenetics.core.Genet;
import lifegenetics.core.Obstacle;
import lifegenetics.core.TerrainType;
import lifegenetics.core.World;
import lifegenetics.core.WorldController;
import lifegenetics.core.WorldElement;

import org.javatuples.Pair;

import utils.WorldMetrics;

import javax.swing.JToggleButton;
import javax.swing.JSlider;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.RowSpec;
import java.awt.Font;

public class GameView extends JFrame implements Observer{

	/** 
	 * Controlador del mundo 
	 */
	private WorldController worldController;
	private boolean isSimulating = true;
	
	// Elements for implementing observation on the world:
	Observable observable;
	private WorldElement[][] currentWorldStatus;
	private TerrainType[][] terrain;
	private static int worldSize;
	
	@SuppressWarnings("unused")
	private long lastInvocationTime;
	
	
	/**
	 * Cosas generadas por Eclipse.
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Panel principal
	 */
	private JPanel contentPane;
	private JPanel panelGrilla;
	private JButton aumentarGrillaButton;
	private JList listView;
	private JButton addGenetButton;
	//private String[] listData;
	private	Vector listData;
	private JToggleButton tglbtnAgua ;
	private JToggleButton tglbtnDesierto ;
	private JToggleButton tglbtnLlanura;
	private JLabel lblGenets, lblStat;
	private JLabel lblVelocidad, lblVida, lblColor, lblCampo, lblDesierto, lblAgua, lblHombres, lblMujeres;
	
	/**
	 * Cosas para la ediciÃ³n de terreno
	 */
	private boolean isEditingTerrain, isDragging;
	private CeldaGrilla startCellForEditing, endCellForEditing;
	private TerrainType selectedTerrain;
	
	/**
	 * Objeto que contiene la configuraciï¿½n de la partida.
	 */
	private static GameConfiguration gameConfig;
	public static GameConfiguration getGameConfig() {
		return gameConfig;
	}
	public static void setGameConfig(GameConfiguration gameConfig) {
		GameView.gameConfig = gameConfig;
	}
	
	
	/** Parte de la configuraciï¿½n visual definita. */
	private static final int borderGrilla = 2;
	private static final int numeroPixelesGrilla = 400 + borderGrilla*2;
	private static final int dimentionSizeIncrease = 1;
	
	
	/** Arreglo hasta que se encuentre una manera de accederlos desde la grid. */
	private CeldaGrilla[][] grillaArray;
	private JSlider slider;
	private JLabel lblX;
	private JLabel monedasLabel;
	private JLabel lblgenetsActuales;
	private JLabel lblgenetsTotales;
	private JLabel lblNewLabel;
	

	
/**
 * MÃ©todo de principal comunicaciÃ³n del patrÃ³n Observer.
 */
	@Override
	public void update(Observable obs, Object arg1) 
	{
		// Display current coins
		monedasLabel.setText("Monedas: " +worldController.getCoins());
		addGenetButton.setEnabled(worldController.getCoins() >= 100);
		
		
		WorldMetrics metrics = worldController.getActualMetrics();
		if(metrics != null)
		{
			lblAgua.setText("Agua: " +metrics.getWaterPercent()+"%");
			lblDesierto.setText("Desierto: " +metrics.getDesertPercent() +"%");
			lblCampo.setText("Campo: " +metrics.getPlainPercent() +"%");
			
			lblHombres.setText("Hombres: " +metrics.getMalePercent() +"%");
			lblMujeres.setText("Mujeres: " +metrics.getFemalePercent() +"%");
			
			lblgenetsActuales.setText("# Genets actuales: " +worldController.getActualGenetsCount());
			lblgenetsTotales.setText("# Genets totales: " +metrics.getGenetCount());
			
			lblVida.setText("Vida: " +metrics.getMeanLife());
			lblVelocidad.setText("Velocidad: " +metrics.getMeanSpeed());
			lblColor.setText("Color: " +metrics.getMeanColor());
		}
		
				
		ArrayList<Pair<Integer, Integer>> coordinatesToUpdate = new ArrayList<Pair<Integer, Integer>>();
		if (arg1 instanceof ArrayList)
		{
			coordinatesToUpdate = (ArrayList<Pair<Integer, Integer>>) arg1;
		}
		else if (arg1 instanceof String)
		{
			ShowOnSideScroll((String) arg1);
			return;
		}
		else
		{
			return;
		}

		
		//ShowOnSideScroll("Hola");
		
		// Now we temporarily configure terrain like this, because terrain generator isn't ready.
		int tamanoDelMundo = gameConfig.getWorldSize();

		// Update. We should fetch information from the World for a certain time interval?
		if (obs instanceof World)
		{
			World world = (World)obs;
			this.currentWorldStatus = world.getWorld();
			this.terrain = world.getTerrain();
			worldSize = world.getHeight();
			
			// If the terrains are changed we should change it accordingly.
			// But now that we haven't implemented any function to change terrain yet, just leave it here.
//			for (int x = 0; x < tamanoDelMundo; x++)
//				for (int y = 0; y < tamanoDelMundo; y++)
//					setTerrainOnCoords(x, y, terrain[x][y]);
			
			// To update the genet representation. Now only the representation of whether they exist or not.
//			for (int x = 0; x < tamanoDelMundo; x++)
//				for (int y = 0; y < tamanoDelMundo; y++)
			for (Pair<Integer, Integer> coordinate : coordinatesToUpdate)
			{
				int x = coordinate.getValue0();
				int y = coordinate.getValue1();
				{
					if (currentWorldStatus[x][y] == null)
					{
						if(grillaArray != null){
							grillaArray[x][y].setExistsElement(false);
							grillaArray[x][y].setExistingGenet(null);
							grillaArray[x][y].setExistingObstacle(null);
							grillaArray[x][y].repaint();
							grillaArray[x][y].validate();
						}
					}
					else
					{
						grillaArray[x][y].setExistsElement(true);
						if (currentWorldStatus[x][y] instanceof Genet)
						{
							int grade = ((Genet) (currentWorldStatus[x][y])).getSkinColor();
							grillaArray[x][y].setColorOfElement(grade);
							grillaArray[x][y].setExistingGenet((Genet) (currentWorldStatus[x][y]));
						}
						else if (currentWorldStatus[x][y] instanceof Obstacle)
						{
							int grade = 255;
							grillaArray[x][y].setColorOfElement(grade);
							grillaArray[x][y].setExistingObstacle((Obstacle) (currentWorldStatus[x][y]));
						}
						grillaArray[x][y].repaint();
						
						grillaArray[x][y].validate();
					}
				}	
			}
		}
	}

	/**
	 * Shows on the sidebar log a message
	 * @param mensaje Message to show.
	 */
	private void ShowOnSideScroll(String mensaje) {
		//listData.addElement( mensaje );
		listData.add(0, mensaje);
		listView.setListData( listData );
	}
	
	/**
	 * Celdas a usar en la grilla, heredan de la clase visual mï¿½s bï¿½sica.
	 */
	private static class CeldaGrilla extends JPanel
	{
		
		/**
		 * Generado por Eclipse
		 */
		private static final long serialVersionUID = 1L;

		// Used for drawing components
		private Boolean existsElement = false;
		private Color colorOfElement = Color.black;
		private Image imagen;
		private int i,j;
		private Genet genet;
		private Obstacle obstacle;
		
		public void setExistingObstacle(Obstacle obs){
			obstacle = obs;
		}
		
		public void setExistingGenet(Genet g){
			genet = g;
		}
		
		public void setExistsElement(Boolean value)
		{
			existsElement = value;
		}

		public void setColorOfElement(int grade)
		{
			colorOfElement = new Color(grade, grade, grade);
			//this.setBackground(colorOfElement);
			}
		
		public void setAsDead()
		{
			this.setBackground(Color.red);
		}
		
	    public void setImagen(String nombreImagen) {
	        if (nombreImagen != null) {
	            imagen = new ImageIcon(
	                   getClass().getResource(nombreImagen)
	                   ).getImage();
	        } else {
	            imagen = null;
	        }
	 
	        repaint();
	    }
	 
	    public void setImagen(Image nuevaImagen) {
	        imagen = nuevaImagen;
	 
	        repaint();
	    }
		
		@Override
	    public void paint(Graphics g) {
	        if (imagen != null) {
	            g.drawImage(imagen, 0, 0, getWidth(), getHeight(),
	                              this);
	 
	            setOpaque(false);
	        } else {
	            setOpaque(true);
	        }
	 
	        super.paint(g);
	    }
	 
		/**
		 * Constructor de la celda.
		 * @param i ï¿½ndice de la celda.
		 */
        public CeldaGrilla(int i, int j) 
        {
            super();
            this.setOpaque(true);
            this.setMinimumSize(new Dimension(1,1));
            this.i = i;
            this.j = j;
            int dimensionGrilla = getGameConfig().getWorldSize();
            int n = i + j*dimensionGrilla; // es como decir la n-ï¿½sima celda agregada.
            if ((n / dimensionGrilla + n % dimensionGrilla) % 2 == 1) 
            {
                this.setBackground(Color.lightGray);
                
            }   
            
            existsElement = false;
        }
        
        public int getI(){
        	return i;
        }
        public int getJ(){
        	return j;
        }
        
        public int getTransformedI(int ntI){
            int transformed = ntI%worldSize;
    		if (transformed < 0)
    		{
    			return transformed+worldSize;
    		}
    		else
    		{
    			return transformed;
    		}
    	}
        
        public int getTransformedJ(int ntJ){
            int transformed = ntJ%worldSize;
    		if (transformed < 0)
    		{
    			return transformed+worldSize;
    		}
    		else
    		{
    			return transformed;
    		}
    	}
        
        // This is for painting the genets. Currently we just represent them as circles.
        
        protected void paintComponent(Graphics g)
        {
        	Graphics2D g2d = (Graphics2D)g;        
        	super.paintComponent(g2d);    
        	if (existsElement && genet != null)
        	{
        		super.paintComponent(g2d);
        		
        		// Esto es para dibujar
        		int posicionIGenet = genet.getI();
				int posicionJGenet = genet.getJ();
				int genetWidth = genet.getWidth();
				
        		BufferedImage img = null;
        		Image image = null;
        		
        		if(getTransformedI(posicionIGenet) == i){
            		img = paintGenetWithImageHorizontalStart(posicionJGenet);
        		}
        		else if(getTransformedI(posicionIGenet + genetWidth - 1) == i)
        		{
        			img = paintGenetWithImageHorizontalEnd(posicionJGenet);
        		}
        		else
        		{
        			img = paintGenetWithImageOtherCases(posicionJGenet);
        		}
        		
        		RGBImageFilter filter = new RGBImageFilter() {
        			public int markerRGB = Color.white.getRGB();
        			public final int filterRGB(int x, int y, int rgb) {
        				if ((rgb) == markerRGB) {
        					return colorOfElement.getRGB();
        				} else {
        					return rgb;
        				}
        			}
        		};
        		
        		if(img != null){
        			ImageProducer ip = new FilteredImageSource(img.getSource(), filter);
            		image = Toolkit.getDefaultToolkit().createImage(ip);
        			g2d.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        		}

        	}
        	else if(existsElement && obstacle != null)
        	{
        		super.paintComponent(g2d);
        		
        		// Esto es para dibujar
        		int posicionIObstacle = obstacle.getI();
				int posicionJObstacle = obstacle.getJ();
				int obstacleWidth = obstacle.getWidth();
				
        		BufferedImage img = null;
        		Image image = null;
        		image = paintObstacleWithImageOtherCases(obstacle.esFabrica());
        		

    			g2d.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        	}
        	else
        	{super.paintComponent(g2d);}
        }
        
        private Image paintObstacleWithImageOtherCases(boolean esFabrica) {
        	Image image = null;
        	try 
        	{
    			if(obstacle.esFabrica())
    			{
    				image = ImageIO.read(getClass().getResource("/images/fabrica.png"));
    			} 
    			else 
    			{
    				image = ImageIO.read(getClass().getResource("/images/obstaculo.png"));
    			}
    		} 
        	catch (IOException e)
        	{
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
			return image;
		}

		protected BufferedImage paintGenetWithImageHorizontalStart(int posicionJGenet){
        	BufferedImage img = null;
        	if(getTransformedJ(posicionJGenet) == j){
        		try {
        			img = ImageIO.read(getClass().getResource("/images/genet0.png"));     
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	else if(getTransformedJ(posicionJGenet + 1) == j)
        	{
        		try {
        			img = ImageIO.read(getClass().getResource("/images/genet2.png"));     
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	else if(getTransformedJ(posicionJGenet + 1) < j)
        	{
        		try {
        			img = ImageIO.read(getClass().getResource("/images/genetLeftFoot.png"));     
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	return img;
        }
        
        protected BufferedImage paintGenetWithImageHorizontalEnd(int posicionJGenet){
        	BufferedImage img = null;
        	if(getTransformedJ(posicionJGenet) == j){
        		try {
        			img = ImageIO.read(getClass().getResource("/images/genet1.png"));     
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	else if(getTransformedJ(posicionJGenet + 1) == j)
        	{
        		try {
        			img = ImageIO.read(getClass().getResource("/images/genet3.png"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        	else if(getTransformedJ(posicionJGenet + 1) < j)
        	{
        		try {
        			img = ImageIO.read(getClass().getResource("/images/genetRightFoot.png"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        	return img;
        }
    
        protected BufferedImage paintGenetWithImageOtherCases(int posicionJGenet){
        	BufferedImage img = null;
        	if(getTransformedJ(posicionJGenet) == j){
        		try {
        			img = ImageIO.read(getClass().getResource("/images/genetHalfUp.png"));     
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	else if(getTransformedJ(posicionJGenet + 1) == j){
        		try {
        			img = ImageIO.read(getClass().getResource("/images/genetHalfDown.png"));     
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	else
        	{
        		try {
        			img = ImageIO.read(getClass().getResource("/images/empty.png"));     
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	return img;
        }
	}
	
	
	public void setObservable(Observable observable)
	{
		this.observable = observable;
	}
	
	
	/**
	 * Create the frame.
	 */
	public GameView(GameConfiguration gameConfiguration)
	{
		GameView.setGameConfig(gameConfiguration);
		isEditingTerrain = false;
		worldController = new WorldController(gameConfiguration);
		
		// Cosas de la ventana principal
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 850, 600);
		setResizable(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Lifegenetics");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		// Cosas relacionadas al top menu.
		JMenuBar menuBar_1 = new JMenuBar();
		configurarJMenuBar(menuBar_1);
		setJMenuBar(menuBar_1);
		
		// Cosas relacionadas a la grilla principal
		panelGrilla = new JPanel();
		panelGrilla.setPreferredSize(new Dimension(numeroPixelesGrilla, numeroPixelesGrilla));
		panelGrilla.setBackground(Color.DARK_GRAY);
		panelGrilla.setBorder(BorderFactory.createEmptyBorder(borderGrilla,borderGrilla,borderGrilla,borderGrilla));
		
		addGenetButton = new JButton("Agregar Genet ($100)");
		addGenetButton.setEnabled(false);
		addGenetButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				CreateGenetView gview = new CreateGenetView(worldController);
				gview.setVisible(true);

				//worldController.createGenetByCoin();
			}
		});
		
		tglbtnAgua = new JToggleButton("Agua");
		tglbtnAgua.setEnabled(false);
		tglbtnAgua.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ShowOnSideScroll("MantÃ©n click y selecciona las celdas que quieres modificar.");
				selectedTerrain = TerrainType.WATER;
				isEditingTerrain = tglbtnAgua.isSelected();
				tglbtnDesierto.setSelected(false);
				tglbtnLlanura.setSelected(false);
			}
		});
		
		tglbtnDesierto = new JToggleButton("Desierto");
		tglbtnDesierto.setEnabled(false);
		tglbtnDesierto.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ShowOnSideScroll("MantÃ©n click y selecciona las celdas que quieres modificar.");
				selectedTerrain = TerrainType.DESERT;
				isEditingTerrain = tglbtnDesierto.isSelected();
				tglbtnAgua.setSelected(false);
				tglbtnLlanura.setSelected(false);
			}
		});

		tglbtnLlanura = new JToggleButton("Llanura");
		tglbtnLlanura.setEnabled(false);
		tglbtnLlanura.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ShowOnSideScroll("MantÃ©n click y selecciona las celdas que quieres modificar.");
				selectedTerrain = TerrainType.PLAIN;
				isEditingTerrain = tglbtnLlanura.isSelected();
				tglbtnAgua.setSelected(false);
				tglbtnDesierto.setSelected(false);
			}
		});
		
		lblX = new JLabel("x1.0");
		
		slider = new JSlider(JSlider.HORIZONTAL, 1, 20, 10);
		slider.setEnabled(false);
		slider.setPaintTicks(true);
		slider.setMinorTickSpacing(1);
		slider.setMajorTickSpacing(10);
		slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				double s = ((double)slider.getValue())/10;
				lblX.setText("x" +s);
				worldController.setTimeMultipler(s);
			}
		});
		
		// Boton de inicio de simulacion
		final JButton btnIniciar = new JButton("Iniciar");
		btnIniciar.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				aumentarGrillaButton.setEnabled(true);
				tglbtnAgua.setEnabled(true);
				tglbtnDesierto.setEnabled(true);
				tglbtnLlanura.setEnabled(true);
				slider.setEnabled(true);
				
				iniciarSimulacion();
				btnIniciar.setText("Detener");
				btnIniciar.removeActionListener(this);
				
				btnIniciar.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						if(isSimulating) // Estaba corriendo la simulaciÃ³n y la detenemos
						{
							btnIniciar.setText("Continuar");
							detenerSimulacion();
						}
						else  //EstÃ¡ detenido y la continuamos
						{
							btnIniciar.setText("Detener");
							continuarSimulacion();
						}
						isSimulating = !isSimulating;
					}
				});
			}
		});
		
		JSeparator separator = new JSeparator();
		
		aumentarGrillaButton = new JButton("Aumentar tamaño de la grilla");
		aumentarGrillaButton.setEnabled(false);
		aumentarGrillaButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				detenerSimulacion();
				int newSize = gameConfig.getWorldSize() + dimentionSizeIncrease;
				gameConfig.setWorldSize(newSize);
				grillaArray = null;
				configurarGrilla(newSize);
				TerrainType[][] t = worldController.expandWorld(newSize);
				for(int i = 0; i < newSize; i++){
					for(int j = 0; j < newSize; j++){
						setTerrainOnCoords(i, j, t[i][j]);
					}
				}
				WorldElement[][] temporaryWorldStatus = new WorldElement[newSize][newSize];
				for(int x = 0; x < newSize; x++)
				{
					for(int y = 0; y < newSize; y++)
					{
						if(x == newSize - 1 || y == newSize - 1)
						{
							temporaryWorldStatus[x][y] = null;
						}
						else
						{
							temporaryWorldStatus[x][y] = currentWorldStatus[x][y];
						}
					}
				}
				
				currentWorldStatus = temporaryWorldStatus;

				for (int x = 0; x < newSize; x++)
				{
					for(int y = 0; y < newSize; y++)
					{
						if (currentWorldStatus[x][y] == null)
						{
							grillaArray[x][y].setExistsElement(false);
							grillaArray[x][y].setExistingGenet(null);
							grillaArray[x][y].setExistingObstacle(null);
							grillaArray[x][y].repaint();
							grillaArray[x][y].validate();
						}
						else
						{
							paintSelectedElements(currentWorldStatus, x, y);
							
						}
					}					
				}
				continuarSimulacion();
			}

			private void paintSelectedElements(WorldElement[][] currentWorldStatus, int x, int y) {
				grillaArray[x][y].setExistsElement(true);
				if (currentWorldStatus[x][y] instanceof Genet)
				{
					int grade = ((Genet) (currentWorldStatus[x][y])).getSkinColor();
					grillaArray[x][y].setColorOfElement(grade);
					grillaArray[x][y].setExistingGenet((Genet) (currentWorldStatus[x][y]));
				}
				else if (currentWorldStatus[x][y] instanceof Obstacle)
				{
					int grade = 255;
					grillaArray[x][y].setColorOfElement(grade);
					grillaArray[x][y].setExistingObstacle((Obstacle) (currentWorldStatus[x][y]));
				}
				grillaArray[x][y].repaint();
				
				grillaArray[x][y].validate();
			}

		});
		
		JSeparator separator_1 = new JSeparator();
		
		listData = new Vector();
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblTiempo = new JLabel("Tiempo:");
		
		JPanel panel = new JPanel();
		
		lblNewLabel = new JLabel("Modificar terreno:");
		lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		
		JPanel panel_3 = new JPanel();
		
		
		
		
		
		// Generado por la IDE
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(separator_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(525))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE)
								.addComponent(panelGrilla, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(6)
									.addComponent(lblTiempo)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(slider, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(lblX)
									.addGap(23))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblNewLabel)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tglbtnLlanura, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tglbtnDesierto, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
									.addGap(3)
									.addComponent(tglbtnAgua, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
									.addContainerGap())
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
										.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
										.addComponent(btnIniciar, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
										.addComponent(separator_1, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
										.addComponent(aumentarGrillaButton, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
										.addComponent(separator, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
										.addComponent(addGenetButton, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
										.addComponent(panel_3, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE))
									.addContainerGap())))))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnIniciar, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblTiempo)
								.addComponent(lblX)
								.addComponent(slider, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(aumentarGrillaButton)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(addGenetButton)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(separator_1, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE))
						.addComponent(panelGrilla, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(separator_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
							.addComponent(tglbtnAgua)
							.addComponent(tglbtnDesierto)
							.addComponent(tglbtnLlanura)
							.addComponent(lblNewLabel))
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
					.addGap(6))
		);
		panel_3.setLayout(new GridLayout(3, 3, 0, 0));
		
		
		lblVelocidad = new JLabel("Velocidad: ");
		panel_3.add(lblVelocidad);
		
		lblVida = new JLabel("Vida: ");
		panel_3.add(lblVida);
		
		lblColor = new JLabel("Color:");
		panel_3.add(lblColor);
		
		 lblCampo = new JLabel("Campo:");
		panel_3.add(lblCampo);
		
		 lblDesierto = new JLabel("Desierto:");
		panel_3.add(lblDesierto);
		
		 lblAgua = new JLabel("Agua:");
		panel_3.add(lblAgua);
		
		 lblHombres = new JLabel("Hombres:");
		panel_3.add(lblHombres);
		
		 lblMujeres = new JLabel("Mujeres");
		panel_3.add(lblMujeres);
		
		monedasLabel = new JLabel("Monedas = 0");
		panel.add(monedasLabel);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		
		lblgenetsActuales = new JLabel("# Genets actuales = 0");
		panel.add(lblgenetsActuales);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		
		lblgenetsTotales = new JLabel("# Genets totales = 0");
		panel.add(lblgenetsTotales);
		
		
				// Create a new listbox control
				listView = new JList( listData );
				scrollPane.setViewportView(listView);
		contentPane.setLayout(gl_contentPane);
		
		// Creaciï¿½n del contenido de la grilla.
		// Si tira exception ene l Design, usar la linea de cï¿½digo que estï¿½ abajo y descomentarla. 
		configurarGrilla(gameConfiguration.getWorldSize());
	}

	/**
	 * Congifuramos la Grilla, este mÃ©todo se llama durante la primera creaciÃ³n y cuando se expande su tamaÃ±o
	 * @param dimension Cuantas celdas de alto y largo.
	 */
	private void configurarGrilla(int dimension)
	{
		//dimension = 1;
		panelGrilla.setLayout(new GridLayout(dimension, dimension));
		panelGrilla.removeAll();
		grillaArray = new CeldaGrilla[dimension][dimension];
		
		for (int j = 0; j < dimension ; j++) 
		{
			for(int i = 0; i < dimension; i++ )
			{
				final CeldaGrilla square = new CeldaGrilla(i,j);
				grillaArray[i][j] = square;
				panelGrilla.add( square );
				square.addMouseListener(new MouseListener() {

					@Override
					public void mouseReleased(MouseEvent arg0) {
						// TODO Auto-generated method stub
						if(isEditingTerrain == true)
						{

							System.out.println("Released");
							isDragging = false;
							MultipleCellChangesTerraing();
						}
					}

					@Override
					public void mousePressed(MouseEvent arg0) {
						// TODO Auto-generated method stub
						if(isEditingTerrain == true)
						{
							System.out.println("Pressed");
							isDragging = true;
							startCellForEditing = endCellForEditing = square;
						}
					}

					@Override
					public void mouseExited(MouseEvent arg0) {
						// TODO Auto-generated method stub

					}

					@Override
					public void mouseEntered(MouseEvent arg0) {
						// TODO Auto-generated method stub
						//click(arg0, square);
						if(isDragging == true)
						{
							endCellForEditing = square;
						}
					}

					@Override
					public void mouseClicked(MouseEvent arg0) {
						// TODO Auto-generated method stub
						click(arg0, square);
					}

				});
			}
		}
	}
	
	/**
	 * Evento que reacciona al recibir un click en una celda.
	 * @param cell Celda presionada.
	 * @param e Evento gatillado.
	 */
	private void click(MouseEvent e, CeldaGrilla cell) 
	{
		// Verificamos que tipo de click es.
		if(e.isMetaDown())
		{
			// Click derecho
	        secondaryClick(e, cell);
		}
		
		else
		{ 
			// Click izquierdo
			int i = cell.getI();
			int j = cell.getJ();
			
			//Genet genet = worldController.getGenetAt(i, j);
			
			
			//throw new NotImplementedException();
		}
    }
	
	
	/**
	 * Evento que reacciona al recibir un click secundario en una celda.
	 * @param cell Celda presionada.
	 * @param e Evento gatillado.
	 */
	private void secondaryClick(MouseEvent e, CeldaGrilla cell)
	{
		int i = cell.getI();
		int j = cell.getJ();
		
		try
		{
		Genet genet = worldController.getGenetAt(i, j);
		
		if(genet != null) // Es genet
		{
			CeldaGenetPopup popup = new CeldaGenetPopup(genet);
			popup.show(e.getComponent(), e.getX(), e.getY());
		}
		else // Es terreno 
		{
			CeldaTerrenoPopup popup = new CeldaTerrenoPopup(cell);
			popup.show(e.getComponent(), e.getX(), e.getY());
		}
		}
		catch(NullPointerException exp)
		{
			return;
		}
	}
	
	private void MultipleCellChangesTerraing()
	{
		int Xi = Math.min(startCellForEditing.i, endCellForEditing.i) ;
		int Yi = Math.min(startCellForEditing.j, endCellForEditing.j) ;
		
		int Xf = Math.max(startCellForEditing.i, endCellForEditing.i) ;
		int Yf = Math.max(startCellForEditing.j, endCellForEditing.j) ;
		
		for(int i = Xi ; i <= Xf; i++)
		{
			for(int j = Yi ; j <= Yf; j++)
			{
				worldController.setTerrainAt(i, j, selectedTerrain);
				setTerrainOnCoords(i, j, selectedTerrain);
			}
		}
				
		startCellForEditing = endCellForEditing = null;
	}
	
	private class CeldaGenetPopup extends JPopupMenu {
	    
		private static final long serialVersionUID = 1L;
		private Genet genetTemp;
		
		/**
		 * Menu que se muestra al hacer segundo click sobre la celda.
		 * @param x PosiciÃ³n horizontal
		 * @param y PosiciÃ³n vertical
		 */
	    public CeldaGenetPopup(Genet genet){
	    	genetTemp = genet;
	    	
	    	JMenuItem item = new JMenuItem("Posición: (" +genet.getI() +" , " +genet.getJ() +") => Genet");
	    	item.setEnabled(false);
	        add(item);
	        
	        JSeparator separador = new Separator();
	        add(separador);
	        
	        JMenuItem item2 = new JMenuItem("Vida Actual: " +genet.getActualLife());// +" | " +genet.getGenotype().getLifeSpanGen().toString());
	        add(item2);
	        
	        JMenuItem item3 = new JMenuItem("Esperanza de Vida: " +genet.getLifeSpan() +" | " +genet.getGenotype().getLifeSpanGen().toString());
	        add(item3);
	        
	        double color = (genet.getSkinColor() / 255.0)*100;
	        JMenuItem item4 = new JMenuItem("Color de piel: " +Math.floor(color) +"% claro | " +genet.getGenotype().getSkinColorGen().toString());
	        add(item4);
	        
	        JMenuItem item5 = new JMenuItem("Velocidad: " +genet.getSpeed() +" | " +genet.getGenotype().getSpeedGen().toString());
	        add(item5);
	     
	        JMenuItem item6 = new JMenuItem("Afinidad con el terreno: " +genet.getTerrainAffinity().toString() +" | " +genet.getGenotype().getTerrainAffinityGen().toString());
	        add(item6);
	        
	        JMenuItem item7 = new JMenuItem("Sexo: " +genet.getSex() +" | " +genet.getGenotype().getSexGen().toString());
	        add(item7);
	        
	        add(new Separator());
	        
	        JMenuItem item8 = new JMenuItem("Migrar a otro mundo");
	        item8.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					// worldController.migrateGenet(genetTemp);
					WorldSelect s = new WorldSelect(genetTemp, worldController);
					s.setVisible(true);
				}
			});
	        add(item8);
	        
	        add(new Separator());
	        
	        JMenuItem item10 = new JMenuItem("Matar Genet");
	        item10.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					// worldController.migrateGenet(genetTemp);
					worldController.killGenet(genetTemp);
					ShowOnSideScroll("Se ha matado a un pobre Genet");
				}
			});
	        add(item10);
	        
	        /* Accion de click
	        item7.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
			*/
	    }
	}
	
	
	private class CeldaTerrenoPopup extends JPopupMenu {
	    
		private static final long serialVersionUID = 1L;
		
		/**
		 * Menu que se muestra al hacer segundo click sobre la celda.
		 * @param x PosiciÃ³n horizontal
		 * @param y PosiciÃ³n vertical
		 */
		CeldaGrilla celdaTemp;
		
	    public CeldaTerrenoPopup(CeldaGrilla cell){
	    	
	    	final CeldaGrilla celdaTemp = cell;
	    	
	    	JMenuItem item = new JMenuItem("Posición: (" +cell.getI() +" , " +cell.getJ() +") => Terreno");
	    	item.setEnabled(false);
	        add(item);
	        
	        JMenuItem item2 = new JMenuItem("Tipo: "+worldController.getTerrainAt(cell.getI(), cell.getJ()));
	        add(item2);
	        
	        JSeparator separador = new Separator();
	        add(separador);
	        
	        
	        JMenuItem item3 = new JMenuItem("PLAIN");
	        add(item3);
	        item3.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					worldController.setTerrainAt(celdaTemp.getI(), celdaTemp.getJ(), TerrainType.PLAIN);
					celdaTemp.getI();
					setTerrainOnCoords(celdaTemp.getI(), celdaTemp.getJ(), TerrainType.PLAIN);
					System.out.println("Terreno Actualizado: PLAIN");

				}
			});
	        
	        JMenuItem item4 = new JMenuItem("WATER");
	        add(item4);
	        item4.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					worldController.setTerrainAt(celdaTemp.getI(), celdaTemp.getJ(), TerrainType.WATER);
					celdaTemp.getI();
					setTerrainOnCoords(celdaTemp.getI(), celdaTemp.getJ(), TerrainType.WATER);
					System.out.println("Terreno Actualizado: WATER");

				}
			});
	        
	        JMenuItem item5 = new JMenuItem("DESERT");
	        add(item5);
	        item5.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					worldController.setTerrainAt(celdaTemp.getI(), celdaTemp.getJ(), TerrainType.DESERT);
					celdaTemp.getI();
					setTerrainOnCoords(celdaTemp.getI(), celdaTemp.getJ(), TerrainType.DESERT);
					System.out.println("Terreno Actualizado: DESERT");

				}
			});
	        

	        
	    
			
	    }
		
	}
	/**
	 * Configuramos la menu bar, la que estÃ¡ arriba.
	 * @param menuBar barra a configurar.
	 */
	private void configurarJMenuBar(JMenuBar menuBar)
	{
		/* Archivo */
		JMenu menuArchivo = new JMenu("Archivo");
		
		/*
		JMenuItem submenuGuardar = new JMenuItem(new AbstractAction("Guardar") {
		    
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
		    	JFileChooser fileChooser = new JFileChooser();
		    	fileChooser.setDialogTitle("Elija donde quiere guardar la partida");   
		    	FileNameExtensionFilter filter = new FileNameExtensionFilter(
		    	        "Partida guardada (XML & JSON)", "xml", "json");
		    	fileChooser.setFileFilter(filter);
		    	int userSelection = fileChooser.showSaveDialog(contentPane);
		    	 
		    	if (userSelection == JFileChooser.APPROVE_OPTION) {
		    	    File fileToSave = fileChooser.getSelectedFile();
		    	    System.out.println("Save as file: " + fileToSave.getAbsolutePath());
		    	    System.out.println("Guardado incompleto");
		    	}
		    }
		});
		menuArchivo.add(submenuGuardar);
		
		menuArchivo.add(new JSeparator());
		*/
		JMenuItem submenuSalir = new JMenuItem(new AbstractAction("Salir") {
		    /**
			 * Generado por Eclipse
			 */
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
		        cerrarPrograma();
		    }
		});
		menuArchivo.add(submenuSalir);
		menuBar.add(menuArchivo);
		
		/*
		JMenu menuOpciones = new JMenu("Genets sugeridos");
		menuBar.add(menuOpciones);
		
		
		JMenu menuAyuda = new JMenu("Ayuda");
		menuBar.add(menuAyuda);
		*/
	}
	
	
	/**
	 * Mï¿½todo que configura una celda de mapa para una cordenada dada.
	 * 
	 * @param x Coordenada eje x
	 * @param y Coordenada eje y
	 */
	public void setTerrainOnCoords(int x, int y, TerrainType terreno)
	{
		CeldaGrilla celda = getCeldaAt(x,y);
		Color color = null;
		switch (terreno) 
		{
		case WATER:
			color = Color.blue;
			celda.setImagen("/images/agua.png");
			break;

		case DESERT:
			color = Color.ORANGE;
			celda.setImagen("/images/desierto.png");
			break;
			
		case PLAIN:
			color = Color.green;
			celda.setImagen("/images/pasto.png");
			break;
			
		default:
			color = Color.lightGray;
			break;
		}
		celda.setBackground(color);
	}
	
	// Method to set the presence of Genet in cells
	// Now temporarily use circle to represent genet.
//	private void setGenet(int x, int y)
//	{
//		CeldaGrilla celda = getCeldaAt(x,y);
//		Graphics2D g2d = new
//		celda.paintComponent(g)
//	}
	
	
	/**
	 * Encuentra la celda del ï¿½ndice pedido.
	 * 
	 * @param x Coordenada eje x.
	 * @param y Coordenada eje y.
	 * @return La celda pedida, en caso de no existir retorna null.
	 */
	private CeldaGrilla getCeldaAt(int x , int y)
	{
		CeldaGrilla celda = null;
		
		/* No funciona: */
		//celda = (CeldaGrilla) panelGrilla.findComponentAt(x, y);
		//celda = (CeldaGrilla) panelGrilla.getComponentAt(x, y);
		//int numeroComponente = y*(tamanoGrilla-1) + x;
		//celda = (CeldaGrilla) panelGrilla.getComponent(numeroComponente);
		
		if(x >= gameConfig.getWorldSize() || y >= gameConfig.getWorldSize())
			celda = null;
		else
			celda = grillaArray[x][y];
		return celda;
	}
	
	
	/**
	 * Busca las coordenadas de la celda.
	 * @param celda Celda a buscar,
	 * @return null si no es encontrado, en caso contrario un arreglo de int's donde el [0] = x & [1] = y.
	 */
	private int[] indexOfCelda(CeldaGrilla celda)
	{
		int j,i = 0;
		for(j = 0; j < gameConfig.getWorldSize(); j++)
		{
			for(i = 0; i < gameConfig.getWorldSize(); i++)
			{
				if(grillaArray[i][j] == celda)
				{
					int[] coords = {i,j};
					return coords;
				}
			}
		}
		return null;
	}
	
	/**
	 * Llama al backend para comenzar por primera vez la simulaciÃ³n
	 */
	private void iniciarSimulacion()
	{
		worldController.startSimulation(this);
	}
	
	/**
	 * Una vez pausada, se continua la simulacion
	 */
	private void continuarSimulacion()
	{
		worldController.continueSimulation();
	}
	
	/**
	 * Se detiene la simulacion, es reanudable por continuarSimulacion()
	 * @see continuarSimulacion()
	 */
	private void detenerSimulacion() 
	{
		worldController.stopSimulation();
	}
	
	/**
	 * Cierra el juego por completo.
	 */
	private void cerrarPrograma(){
		System.exit(0); 
	}	
}
