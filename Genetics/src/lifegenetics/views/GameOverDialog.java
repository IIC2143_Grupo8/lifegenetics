package lifegenetics.views;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.Dialog.ModalityType;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle.ComponentPlacement;

import utils.WorldMetrics;

import javax.swing.JButton;

public class GameOverDialog extends JDialog {

	/**
	 * Create the dialog.
	 */
	public GameOverDialog(WorldMetrics metricsIniciales, WorldMetrics metricsFinales) {
		setBounds(100, 100, 693, 336);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		JLabel lblGameover = new JLabel("GAME OVER");
		lblGameover.setVerticalAlignment(SwingConstants.TOP);
		lblGameover.setHorizontalAlignment(SwingConstants.CENTER);
		lblGameover.setFont(new Font("Consolas", Font.BOLD, 24));
		
		JSeparator separator = new JSeparator();
		
		JLabel lblNewLabel = new JLabel("Velocidad inicial: " +metricsIniciales.getMeanSpeed());
		
		JLabel lblVidaPromedio = new JLabel("Vida inicial: " +metricsIniciales.getMeanLife());
		
		JLabel lblColorPromedio = new JLabel("Color inicial: "+metricsIniciales.getMeanColor());
		
		JLabel lblPorcentajeHombres = new JLabel("Porcentaje hombres inicial: " +metricsIniciales.getMalePercent()+"%");
		
		JLabel lblNewLabel_1 = new JLabel("Porcentaje mujeres inicial: "+metricsIniciales.getFemalePercent() +"%");
		
		JLabel lblNmeroDeGenets = new JLabel("Número de Genets totales: " +metricsFinales.getGenetCount());
		
		final JButton btnNewButton = new JButton("Cerrar ventana");
		btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Container frame = btnNewButton.getParent();
				
				for(int i=0;i < (Frame.getFrames()).length;i++)
				{
					(Frame.getFrames())[i].dispose();
					System.exit(0);
				}
			}
		});
		
		JLabel lblVelocidadFinal = new JLabel("Velocidad final: " +metricsFinales.getMeanSpeed());
		
		JLabel lblVidaFinal = new JLabel("Vida final: " +metricsFinales.getMeanLife());
		
		JLabel lblNewLabel_2 = new JLabel("Color final: "+metricsFinales.getMeanColor());
		
		JLabel lblPorcentajeHombresFinal = new JLabel("Porcentaje hombres final: " +metricsFinales.getMalePercent() +"%");
		
		JLabel lblPorcentajeMujeresFinal = new JLabel("Porcentaje mujeres final: " +metricsFinales.getFemalePercent() +"%");
		
		JLabel lblPorcentajeAfinidadAgua = new JLabel("Porcentaje afinidad agua inicial: " +metricsIniciales.getWaterPercent() +"%");
		
		JLabel lblPorcentajeAfinidadDesierto = new JLabel("Porcentaje afinidad desierto inicial: " +metricsIniciales.getDesertPercent() +"%");;
		
		JLabel lblPorcentajeAfinidadCampo = new JLabel("Porcentaje afinidad campo inicial: " +metricsIniciales.getPlainPercent() +"%");
		
		JLabel lblPorcentajeAfinidadAgua_1 = new JLabel("Porcentaje afinidad agua final: " +metricsFinales.getWaterPercent() +"%");
		
		JLabel lblPorcentajeAfinidadDesierto_1 = new JLabel("Porcentaje afinidad desierto final: " +metricsFinales.getDesertPercent() +"%");;
		
		JLabel lblPorcentajeAfinidadCampo_1 = new JLabel("Porcentaje afinidad campo final: " +metricsFinales.getPlainPercent() +"%");
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 681, Short.MAX_VALUE)
						.addComponent(lblGameover, GroupLayout.DEFAULT_SIZE, 681, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(separator, GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel)
										.addComponent(lblVidaPromedio)
										.addComponent(lblColorPromedio)
										.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
											.addComponent(lblNmeroDeGenets)
											.addComponent(lblPorcentajeHombres))
										.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
											.addComponent(lblPorcentajeAfinidadDesierto, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(lblNewLabel_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addComponent(lblPorcentajeAfinidadAgua)
										.addComponent(lblPorcentajeAfinidadCampo, GroupLayout.PREFERRED_SIZE, 323, GroupLayout.PREFERRED_SIZE))
									.addGap(39)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblPorcentajeMujeresFinal)
										.addComponent(lblNewLabel_2)
										.addComponent(lblVidaFinal)
										.addComponent(lblVelocidadFinal)
										.addComponent(lblPorcentajeHombresFinal)
										.addComponent(lblPorcentajeAfinidadAgua_1, GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
										.addComponent(lblPorcentajeAfinidadDesierto_1, GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
										.addComponent(lblPorcentajeAfinidadCampo_1, GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE))))
							.addGap(10)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(14)
					.addComponent(lblGameover, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(separator, GroupLayout.PREFERRED_SIZE, 7, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblVelocidadFinal)
							.addGap(4)
							.addComponent(lblVidaFinal)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblNewLabel_2)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblPorcentajeHombresFinal)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(lblPorcentajeMujeresFinal))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblNewLabel)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblVidaPromedio)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblColorPromedio)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblPorcentajeHombres)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblNewLabel_1)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblPorcentajeAfinidadAgua_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblPorcentajeAfinidadDesierto_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblPorcentajeAfinidadCampo_1))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblPorcentajeAfinidadAgua)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblPorcentajeAfinidadDesierto)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblPorcentajeAfinidadCampo)
							.addPreferredGap(ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
							.addComponent(lblNmeroDeGenets)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton)
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);

	}
}
