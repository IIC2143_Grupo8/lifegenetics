package lifegenetics.views;

import java.awt.BorderLayout;
import lifegenetics.core.GameConfiguration;
import lifegenetics.core.TerrainType;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JSeparator;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JCheckBox;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class SetupDialog extends JDialog {

	/**
	 * Generado por Eclipse
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Campos para configurar el terreno
	 * Basta cambiar los elementos de los arreglos, ojo con la coherencia.
	 */
	private final TerrainType[] ordenTerreno = {TerrainType.DESERT, TerrainType.WATER, TerrainType.PLAIN};
	private final String[] listaOpcionesTerreno = {"Desierto", "Agua", "Llanura"};
	private JCheckBox[] checkTerrenos = new JCheckBox[listaOpcionesTerreno.length];
	
	/**
	 * Campos para configurar los genes involucrados
	 * Basta cambiar los elementos de los arreglos, ojo con la coherencia.
	 */
	private JCheckBox[] checkGenes = new JCheckBox[GameConfiguration.availableGenList.length];
	
	/**
	 * Elementos visuales
	 */
	private final JPanel contentPanel = new JPanel();
	private JTextField tamanoTextfield;
	private JTextField genetsInicialesTextfield;
	
	/**
	 * Genets iniciales
	 */
	private final int genetsIniciales = 5;
	
	private StartupDialog startupDiag;
	private JTextField obstaculosInicialestextField;


	/**
	 * Create the dialog.
	 */
	public SetupDialog(StartupDialog dialogoPrincipal) 
	{
		startupDiag = dialogoPrincipal;
		setTitle("Configurar partida");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 346, 489);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		setResizable(false);
		
		JLabel lblTamaoDelMundo = new JLabel("Tama\u00F1o del mundo:");
		
		tamanoTextfield = new JTextField();
		tamanoTextfield.setText("20");
		tamanoTextfield.setToolTipText("N\u00FAmero de celdas por ancho y largo");
		tamanoTextfield.setColumns(10);
		tamanoTextfield.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				warn();
			}
			public void removeUpdate(DocumentEvent e) {
				warn();
			}
			public void insertUpdate(DocumentEvent e) {
				warn();
			}
			
			/**
			 * Controlamos lo que pasa al editar el textfield
			 */
			public void warn()
			{
				Color color = null;
				try
				{
					if(tryStringToInt(tamanoTextfield.getText()) >= 5 && tryStringToInt(tamanoTextfield.getText()) <= 100)
						color = Color.white;
					else 
						throw new NumberFormatException();
				}
				catch(NumberFormatException exp)
				{
					color = Color.red;
				}
				finally
				{
					tamanoTextfield.setBackground(color);
				}
			}
		});

		JSeparator separator = new JSeparator();
		
		JLabel lblBsicos = new JLabel("B\u00E1sicos");
		lblBsicos.setForeground(Color.LIGHT_GRAY);
		lblBsicos.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		
		JLabel lblVelocidadTiempo = new JLabel("Genets iniciales:");
		
		genetsInicialesTextfield = new JTextField();
		genetsInicialesTextfield.setToolTipText("Con cuantas entidades se desea partir");
		genetsInicialesTextfield.setText(""+genetsIniciales);
		genetsInicialesTextfield.setColumns(10);
		genetsInicialesTextfield.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				warn();
			}
			public void removeUpdate(DocumentEvent e) {
				warn();
			}
			public void insertUpdate(DocumentEvent e) {
				warn();
			}

			/**
			 * Controlamos lo que pasa al editar el textfield
			 */
			public void warn()
			{
				Color color = null;
				try
				{
					if(tryStringToInt(genetsInicialesTextfield.getText()) >= 1)
						color = Color.white;
					else 
						throw new NumberFormatException();
				}
				catch(NumberFormatException exp)
				{
					color = Color.red;
				}
				finally
				{
					genetsInicialesTextfield.setBackground(color);
				}
			}
		});

		
		JLabel lblAvanzados = new JLabel("Terreno");
		lblAvanzados.setForeground(Color.LIGHT_GRAY);
		lblAvanzados.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		
		JSeparator separator_1 = new JSeparator();
		
		JPanel terrenoOpciones = new JPanel();
		
		JLabel lblGenes = new JLabel("Genes");
		lblGenes.setForeground(Color.LIGHT_GRAY);
		lblGenes.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		
		JSeparator separator_2 = new JSeparator();
		
		JPanel genesOpciones = new JPanel();
		genesOpciones.setLayout(new GridLayout(3, 1, 0, 0));
		
		JLabel lblLosGenesQue = new JLabel("<html>Los genes que no fueron seleccionados usarán <br> un valor por defecto y constante");
				//new JLabel("Los genes que no seleccionados usar\u00E1n un valor por defecto y constante");
		lblLosGenesQue.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
		
		JLabel lblObstaculosIniciales = new JLabel("Obstaculos iniciales:");
		
		obstaculosInicialestextField = new JTextField();
		obstaculosInicialestextField.setToolTipText("Con cuantas entidades se desea partir");
		obstaculosInicialestextField.setText("5");
		obstaculosInicialestextField.setColumns(10);
		obstaculosInicialestextField.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				warn();
			}
			public void removeUpdate(DocumentEvent e) {
				warn();
			}
			public void insertUpdate(DocumentEvent e) {
				warn();
			}

			/**
			 * Controlamos lo que pasa al editar el textfield
			 */
			public void warn()
			{
				Color color = null;
				try
				{
					if(tryStringToInt(obstaculosInicialestextField.getText()) >= 1)
						color = Color.white;
					else 
						throw new NumberFormatException();
				}
				catch(NumberFormatException exp)
				{
					color = Color.red;
				}
				finally
				{
					obstaculosInicialestextField.setBackground(color);
				}
			}
		});
		
		
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(7)
					.addComponent(lblBsicos)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(separator, GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
					.addGap(4))
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(lblTamaoDelMundo, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(tamanoTextfield, GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addComponent(lblVelocidadTiempo, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
							.addGap(12)
							.addComponent(genetsInicialesTextfield, GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)))
					.addGap(1))
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblLosGenesQue, GroupLayout.PREFERRED_SIZE, 324, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblAvanzados)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(separator_1, GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblObstaculosIniciales, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(obstaculosInicialestextField, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(terrenoOpciones, GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblGenes, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(separator_2, GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
					.addGap(7))
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addComponent(genesOpciones, GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblBsicos))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTamaoDelMundo)
						.addComponent(tamanoTextfield, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGap(7)
							.addComponent(lblVelocidadTiempo))
						.addComponent(genetsInicialesTextfield, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(obstaculosInicialestextField, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblObstaculosIniciales))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(separator_1)
						.addComponent(lblAvanzados, GroupLayout.DEFAULT_SIZE, 13, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(terrenoOpciones, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblGenes, GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE)
						.addComponent(separator_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(genesOpciones, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
					.addGap(98)
					.addComponent(lblLosGenesQue, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		terrenoOpciones.setLayout(new GridLayout(listaOpcionesTerreno.length, 1, 0, 0));
		
		// Creamos las checkbox
		for(int i = 0; i < listaOpcionesTerreno.length; i++)
		{
			JCheckBox opcion =  new JCheckBox(listaOpcionesTerreno[i]);
			opcion.setMinimumSize(new Dimension(100, 30));
			opcion.setMaximumSize(new Dimension(100000000,30));
			opcion.setSelected(true);
			checkTerrenos[i] = opcion;
			terrenoOpciones.add(opcion);
		}
		
		// Creamos las checkbox
		for(int i = 0; i < GameConfiguration.availableGenList.length; i++)
		{
			JCheckBox opcion =  new JCheckBox(GameConfiguration.availableGenList[i]);
			opcion.setMinimumSize(new Dimension(100, 30));
			opcion.setMaximumSize(new Dimension(100000000,30));
			opcion.setSelected(true);
			checkGenes[i] = opcion;
			genesOpciones.add(opcion);
		}
		
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Iniciar");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						iniciarJuego();
					}
				});
			}
			{
				JButton cancelButton = new JButton("Cancelar");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						dispose();
					}
				});
			}
		}

		
	}

	/**
	 * Se ejecuta al hacer click en Iniciar, si los par�metros están incorrectos no termina la ejecución del método.
	 */
	private void iniciarJuego()
	{
		int tamanoMundo, genetsIniciales, obstaculosIniciales;
		try
		{
			obstaculosIniciales = tryStringToInt(obstaculosInicialestextField.getText());
			tamanoMundo = tryStringToInt(tamanoTextfield.getText());
			genetsIniciales = tryStringToInt(genetsInicialesTextfield.getText());
		}
		catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(null, "Uno o más de los parámetros no respeta el formato de número entero positivo", "Error", JOptionPane.ERROR_MESSAGE  );
			return;
		}
		
		ArrayList<TerrainType> tiposSeleccionados = new ArrayList<TerrainType>();
		for(int i = 0; i < checkTerrenos.length; i++)
		{
			JCheckBox chkbx = checkTerrenos[i];
			if(chkbx.isSelected())
				tiposSeleccionados.add(ordenTerreno[i]);
		}
		
		ArrayList<String> genesSeleccionados = new ArrayList<String>();
		for(int i = 0; i < checkGenes.length; i++)
		{
			JCheckBox chkbx = checkGenes[i];
			if(chkbx.isSelected())
				genesSeleccionados.add(chkbx.getText());
		}
		
		GameConfiguration gameConfig = new GameConfiguration(tamanoMundo, genetsIniciales, obstaculosIniciales, tiposSeleccionados, genesSeleccionados);
		GameView game = new GameView(gameConfig);
		game.setVisible(true);
		setVisible(false);
		dispose();
		startupDiag.setVisible(false);
		startupDiag.dispose();
	}
	
	/**
	 * Intenta transformar un string en un int
	 * @param s String a convertir
	 * @return Entero obtenido
	 * @throws NumberFormatException En caso de que no se pueda convertir o sea un número negativo.
	 */
	private int tryStringToInt(String s) throws NumberFormatException
	{
		int n = Integer.parseInt(s);
		if(n <= 0)
			throw new NumberFormatException();
		return n;
	}
}
