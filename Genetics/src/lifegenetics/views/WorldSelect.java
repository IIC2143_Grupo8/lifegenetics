package lifegenetics.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import lifegenetics.core.Genet;
import lifegenetics.core.WorldController;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;


public class WorldSelect extends JDialog {

	private JPanel contentPane;
	private Genet travelingGenet;

	/**
	 * Create the frame.
	 */
	public WorldSelect(Genet genet, final WorldController world) {
		setTitle("Migrar Genet");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		travelingGenet = genet;
		
		setBounds(100, 100, 344, 98);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblSeleccionaElMundo = new JLabel("<html>Selecciona el mundo donde será enviado el Genet.");
		
		List<String> mundos = world.getListaDeMundos();
		
		final JComboBox<String> comboBox = new JComboBox<String>();
		JButton btnEnviar = new JButton("Enviar");
		
		for(int i = 0; i <mundos.size(); i++)
		{
			String titulo = mundos.get(i);
			comboBox.addItem(titulo);
		}
		
		btnEnviar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(travelingGenet == null) //|| travelingGenet.isAlive() == true)
				{
					JOptionPane.showMessageDialog(null, "El Genet ha dejado de existir mientras seleccionabas un mundo.", "Error", JOptionPane.ERROR_MESSAGE  );
					setVisible(false);
					dispose();
				}
				else
				{
					int index = comboBox.getSelectedIndex();
					world.migrateGenet(travelingGenet, index);
					setVisible(false);
					dispose();
				}
			}
		});
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(6)
							.addComponent(lblSeleccionaElMundo, GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
							.addComponent(comboBox, 0, 240, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnEnviar)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSeleccionaElMundo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnEnviar))
					.addContainerGap(41, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
		
		
		
	}
}
