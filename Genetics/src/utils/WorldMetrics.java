package utils;

import java.text.DecimalFormat;
import java.util.ArrayList;

import lifegenetics.core.Genet;

public class WorldMetrics {	
	private int genetCount;
	private float meanLife;
	private float meanSpeed;
	private float meanColor;
	private float maleCount;
	private float femaleCount;
	private float desertCount;
	private float waterCount;
	private float plainCount;
	
	public WorldMetrics()
	{
	}
	
	public WorldMetrics(ArrayList<Genet> genets)
	{
		for(int i=0;i<genets.size();i++)
		{
			update(genets.get(i));
		}
	}
	
	public void update(Genet genet)
	{
		meanLife = (meanLife*genetCount + genet.getLifeSpan())/(genetCount+1);
		meanSpeed = (meanSpeed*genetCount + genet.getSpeed())/(genetCount+1);
		meanColor = (meanColor*genetCount + genet.getSkinColor())/(genetCount+1);
		switch(genet.getSex())
		{
			case FEMALE:
				femaleCount++;
				break;
			case MALE:
				maleCount++;
				break;
		}
		switch(genet.getTerrainAffinity())
		{
			case DESERT:
				desertCount++;
				break;
			case PLAIN:
				plainCount++;
				break;
			case WATER:
				waterCount++;
				break;
		}
		genetCount++;
	}

	private String twoDecimal(float f)
	{
		return String.format("%.1f", f);
	}
	
	public int getGenetCount()
	{
		return genetCount;
	}
	
	public String getMeanLife()
	{
		return twoDecimal(meanLife);
	}
	
	public String getMeanSpeed()
	{
		return twoDecimal(meanSpeed);
	}
	
	public String getMeanColor()
	{
		return twoDecimal(meanColor);
	}
	
	public String getMalePercent()
	{
		return twoDecimal(maleCount/genetCount * 100);
	}
	
	public String getFemalePercent()
	{
		return twoDecimal((1-(maleCount/genetCount)) * 100);
	}
	
	public String getDesertPercent()
	{
		return twoDecimal(desertCount/genetCount * 100);
	}
	
	public String getWaterPercent()
	{
		return twoDecimal(waterCount/genetCount * 100);
	}
	
	public String getPlainPercent()
	{
		return twoDecimal((1-(desertCount/genetCount)-(waterCount/genetCount))*100);
	}
	
	public WorldMetrics clone()
	{
		WorldMetrics clone = new WorldMetrics();
		clone.desertCount = desertCount;
		clone.femaleCount = femaleCount;
		clone.genetCount = genetCount;
		clone.maleCount = maleCount;
		clone.meanColor = meanColor;
		clone.meanLife = meanLife;
		clone.meanSpeed = meanSpeed;
		clone.plainCount = plainCount;
		clone.waterCount = waterCount;
		return clone;
	}
}
