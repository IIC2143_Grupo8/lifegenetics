package utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class WorldConnectionConfigLoader {
	
	static public List<WorldConnectionConfig> loadConfig(String path)
	{
		List<WorldConnectionConfig> worlds = new ArrayList<WorldConnectionConfig>();
		try{
			File fXmlFile = new File(path);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			
			NodeList nList = doc.getElementsByTagName("World");
			
			for(int i = 0; i < nList.getLength(); i++) {
				 
				Node nNode = nList.item(i);
				
				Element world = (Element) nNode;
				
				String name = world.getAttribute("name");
				String ip = world.getAttribute("ip");
				int port = Integer.parseInt(world.getAttribute("port"));
				
				WorldConnectionConfig config = new WorldConnectionConfig(name, ip.split(","), port);
				worlds.add(config);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
	    }
		
		return worlds;
	}

}
